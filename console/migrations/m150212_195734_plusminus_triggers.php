<?php

use yii\db\Schema;
use yii\db\Migration;

class m150212_195734_plusminus_triggers extends Migration
{
    public function up()
    {
        $this->execute(<<<SQL

 CREATE TRIGGER insert_plusminus BEFORE INSERT ON plus_minus
 FOR EACH ROW
 BEGIN
     IF NEW.sign = 'plus' THEN
         UPDATE post SET pluses_count = pluses_count + 1 WHERE id = NEW.post_id;
     ELSEIF NEW.sign = 'minus' THEN
         UPDATE post SET minuses_count = minuses_count + 1 WHERE id = NEW.post_id;
     END IF;
 END;

 CREATE TRIGGER delete_plusminus BEFORE DELETE ON plus_minus
 FOR EACH ROW
 BEGIN
     IF OLD.sign = 'plus' THEN
         UPDATE post SET pluses_count = pluses_count - 1 WHERE id = OLD.post_id;
     ELSEIF OLD.sign = 'minus' THEN
         UPDATE post SET minuses_count = minuses_count - 1 WHERE id = OLD.post_id;
     END IF;
 END;
                   
SQL
                       );

    }

    public function down()
    {
        echo "m150212_195734_plusminus_triggers cannot be reverted.\n";

        return false;
    }
}
