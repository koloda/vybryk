<?php

use yii\db\Schema;
use yii\db\Migration;

class m150304_160144_alter_tag_to_post_table extends Migration
{
    private $tableName = 'tag_to_post';
    private $columnName = 'attach_time';

    public function safeUp()
    {
        $this->addColumn($this->tableName, $this->columnName, Schema::TYPE_DATETIME);
    }

    public function safeDown()
    {
        $this->dropColumn($this->tableName, $this->columnName);
    }
}
