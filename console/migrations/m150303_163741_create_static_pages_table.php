<?php

use yii\db\Schema;
use yii\db\Migration;

class m150303_163741_create_static_pages_table extends Migration
{
    private $tableName = 'static_page';

    public function safeUp()
    {
        $this->createTable(
            $this->tableName,
            [
                'id' => Schema::TYPE_PK,
                'title' => Schema::TYPE_STRING . ' NOT NULL',
                'content' => Schema::TYPE_TEXT
            ],
            'ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci'
        );

        $this->createIndex('i_static_page_title', $this->tableName, 'title', true);
    }
    
    public function safeDown()
    {
        $this->dropIndex('i_static_page_title', $this->tableName);
        $this->truncateTable($this->tableName);
        $this->dropTable($this->tableName);
    }
}
