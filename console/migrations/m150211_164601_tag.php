<?php

use yii\db\Schema;
use yii\db\Migration;

class m150211_164601_tag extends Migration
{
    public function up()
    {
        $this->execute(<<<SQL

CREATE TABLE `tag` ( 
    `id` Int( 16 ) AUTO_INCREMENT NOT NULL,
    `text` VarChar( 128 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL, 
    `posts_count` Int( 16 ) NULL DEFAULT  '0',
     PRIMARY KEY ( `id` )
 );
CREATE INDEX `post_id` ON `tag`( `post_id` )

CREATE INDEX `tag_text` ON `tag`( `text` );

SQL
       );

    }

    public function down()
    {
        echo "m150211_164601_tag cannot be reverted.\n";

        return false;
    }
}
