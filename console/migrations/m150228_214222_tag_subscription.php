<?php

use yii\db\Schema;
use yii\db\Migration;

class m150228_214222_tag_subscription extends Migration
{
    public function up()
    {
        $this->execute(<<<SQL

CREATE TABLE IF NOT EXISTS `tag_subscription` (
  `tag_id` int(16) NOT NULL,
  `user_id` int(11) NOT NULL,
  UNIQUE KEY `subscription` (`tag_id`,`user_id`),
  KEY `tag` (`tag_id`),
  KEY `user` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE  `tag_subscription` ADD FOREIGN KEY (  `tag_id` ) REFERENCES  `tag` (
`id`
) ON DELETE CASCADE ON UPDATE CASCADE ;

ALTER TABLE  `tag_subscription` ADD FOREIGN KEY (  `user_id` ) REFERENCES  `user` (
`id`
) ON DELETE CASCADE ON UPDATE CASCADE ;

SQL
                       );

    }

    public function down()
    {
        echo "m150228_214222_tag_subscription cannot be reverted.\n";

        return false;
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
