<?php

use yii\db\Schema;
use yii\db\Migration;

class m150211_164951_tag_to_post extends Migration
{
    public function up()
    {
        $this->execute(<<<SQL

CREATE TABLE `tag_to_post` ( 
    `tag_id` Int( 255 ) NOT NULL, 
    `post_id` Int( 16 ) NOT NULL
 );
CREATE INDEX `index_tag_id` ON `tag_to_post`( `tag_id` );

CREATE INDEX `index_field` ON `tag_to_post`( `post_id` );

ALTER TABLE  `tag_to_post` ADD FOREIGN KEY (  `post_id` ) REFERENCES  `post` (
`id`
) ON DELETE CASCADE ON UPDATE CASCADE ;

ALTER TABLE  `tag_to_post` ADD FOREIGN KEY (  `tag_id` ) REFERENCES  `tag` (
`id`
) ON DELETE CASCADE ON UPDATE CASCADE ;

SQL
       );

    }

    public function down()
    {
        echo "m150211_164951_tag_to_post cannot be reverted.\n";

        return false;
    }
}
