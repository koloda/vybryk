<?php

use yii\db\Schema;
use yii\db\Migration;

class m150212_171137_plus_minus extends Migration
{
    public function up()
    {
        $this->execute(<<<SQL

CREATE TABLE IF NOT EXISTS `plus_minus` (
  `post_id` int(16) NOT NULL,
  `user_id` int(11) NOT NULL,
  `sign` enum('plus','minus') NOT NULL,
  UNIQUE KEY `post_id` (`post_id`,`user_id`),
  KEY `post` (`post_id`),
  KEY `user` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE  `plus_minus` ADD FOREIGN KEY (  `post_id` ) REFERENCES  `post` (
`id`
) ON DELETE CASCADE ON UPDATE CASCADE ;

ALTER TABLE  `plus_minus` ADD FOREIGN KEY (  `user_id` ) REFERENCES  `user` (
`id`
) ON DELETE CASCADE ON UPDATE CASCADE ;

SQL
                    );

    }

    public function down()
    {
        echo "m150212_171137_plus_minus cannot be reverted.\n";

        return false;
    }
}
