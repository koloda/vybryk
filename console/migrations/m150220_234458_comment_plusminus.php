<?php

use yii\db\Schema;
use yii\db\Migration;

class m150220_234458_comment_plusminus extends Migration
{
    public function up()
    {
        $this->execute(<<<SQL

CREATE TABLE IF NOT EXISTS `comment_plus_minus` (
  `comment_id` int(16) NOT NULL,
  `user_id` int(11) NOT NULL,
  `sign` enum('plus','minus') NOT NULL,
  UNIQUE KEY `comment_id` (`comment_id`,`user_id`),
  KEY `comment` (`comment_id`),
  KEY `user` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE  `comment_plus_minus` ADD FOREIGN KEY (  `comment_id` ) REFERENCES  `comment` (
`id`
) ON DELETE CASCADE ON UPDATE CASCADE ;

ALTER TABLE  `comment_plus_minus` ADD FOREIGN KEY (  `user_id` ) REFERENCES  `user` (
`id`
) ON DELETE CASCADE ON UPDATE CASCADE ;

SQL
                       );

    }

    public function down()
    {
        echo "m150220_234458_comment_plusminus cannot be reverted.\n";

        return false;
    }
}
