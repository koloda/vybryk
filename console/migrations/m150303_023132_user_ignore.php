<?php

use yii\db\Schema;
use yii\db\Migration;

class m150303_023132_user_ignore extends Migration
{
    public function up()
    {
        $this->execute(<<<SQL

CREATE TABLE IF NOT EXISTS `user_ignore` (
  `ignored_id` int(16) NOT NULL,
  `user_id` int(11) NOT NULL,
  UNIQUE KEY `ignore` (`ignored_id`,`user_id`),
  KEY `ignored` (`ignored_id`),
  KEY `user` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE  `user_ignore` ADD FOREIGN KEY (  `ignored_id` ) REFERENCES  `user` (
`id`
) ON DELETE CASCADE ON UPDATE CASCADE ;

ALTER TABLE  `user_ignore` ADD FOREIGN KEY (  `user_id` ) REFERENCES  `user` (
`id`
) ON DELETE CASCADE ON UPDATE CASCADE ;

SQL
                       );

    }

    public function down()
    {
        echo "m150303_023132_user_ignore cannot be reverted.\n";

        return false;
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
