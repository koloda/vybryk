<?php

use yii\db\Schema;
use yii\db\Migration;

class m150304_162215_alter_tag_to_post_table extends Migration
{
    private $tableName = 'tag_to_post';

    public function safeUp()
    {
        $this->addPrimaryKey('pk', $this->tableName, ['tag_id', 'post_id']);
    }

    public function safeDown()
    {
        $this->dropPrimaryKey('pk', $this->tableName);
    }
}
