<?php

use yii\db\Schema;
use yii\db\Migration;

class m150303_014942_user_subscriptions extends Migration
{
    public function up()
    {
        $this->execute(<<<SQL

CREATE TABLE IF NOT EXISTS `user_subscription` (
  `user_id` int(11) NOT NULL,
  `follower_id` int(16) NOT NULL,
  UNIQUE KEY `subscription` (`follower_id`,`user_id`),
  KEY `follower` (`follower_id`),
  KEY `user` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE  `user_subscription` ADD FOREIGN KEY (  `follower_id` ) REFERENCES  `user` (
`id`
) ON DELETE CASCADE ON UPDATE CASCADE ;

ALTER TABLE  `user_subscription` ADD FOREIGN KEY (  `user_id` ) REFERENCES  `user` (
`id`
) ON DELETE CASCADE ON UPDATE CASCADE ;

SQL
                       );

    }

    public function down()
    {
        echo "m150303_014942_user_subscriptions cannot be reverted.\n";

        return false;
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
