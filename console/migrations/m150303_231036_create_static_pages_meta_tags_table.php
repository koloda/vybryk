<?php

use yii\db\Schema;
use yii\db\Migration;

class m150303_231036_create_static_pages_meta_tags_table extends Migration
{
    private $tableName = 'static_page_meta_tags';

    public function safeUp()
    {
        $this->createTable(
            $this->tableName,
            [
                'id' => Schema::TYPE_PK,
                'name' => Schema::TYPE_STRING . ' NOT NULL',
                'meta' => Schema::TYPE_STRING . ' NOT NULL',
                'static_page_id' => Schema::TYPE_INTEGER . ' NOT NULL'
            ],
            'ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci'
        );

        $this->addForeignKey(
            'fk_meta_tag_static_page', $this->tableName, 'static_page_id', 'static_page', 'id', 'CASCADE', 'CASCADE'
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk_meta_tag_static_page', $this->tableName);
        $this->truncateTable($this->tableName);
        $this->dropTable($this->tableName);
    }
}
