<?php

use yii\db\Schema;
use yii\db\Migration;

class m150209_155804_post extends Migration
{
    public function up()
    {
        $this->execute(<<<SQL

CREATE TABLE IF NOT EXISTS `post` (
  `id` int(16) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) NOT NULL,
  `title` varchar(250) NOT NULL,
  `description` varchar(400) DEFAULT NULL,
  `text` text NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `comments_count` int(9) unsigned DEFAULT '0',
  `pluses_count` int(9) DEFAULT '0',
  `minuses_count` int(9) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

ALTER TABLE  `post` ADD  `careated_date` DATE NOT NULL AFTER  `created` ;

ALTER TABLE  `post` ADD FOREIGN KEY (  `user_id` ) REFERENCES  `user` (
`id`
) ON DELETE CASCADE ON UPDATE CASCADE ;

ALTER TABLE  `post` CHANGE  `title`  `title` VARCHAR( 250 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ,
CHANGE  `description`  `description` VARCHAR( 400 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
CHANGE  `text`  `text` TEXT CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ;

SQL
        );

    }

    public function down()
    {
        echo "m150209_155804_post cannot be reverted.\n";

        return false;
    }
}
