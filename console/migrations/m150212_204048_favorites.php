<?php

use yii\db\Schema;
use yii\db\Migration;

class m150212_204048_favorites extends Migration
{
    public function up()
    {
        $this->execute(<<<SQL

CREATE TABLE IF NOT EXISTS `favorites` (
  `post_id` int(16) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  UNIQUE KEY `post_id` (`post_id`,`user_id`),
  KEY `post` (`post_id`),
  KEY `user` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE  `favorites` ADD FOREIGN KEY (  `post_id` ) REFERENCES  `post` (
`id`
) ON DELETE CASCADE ON UPDATE CASCADE ;

ALTER TABLE  `favorites` ADD FOREIGN KEY (  `user_id` ) REFERENCES  `user` (
`id`
) ON DELETE CASCADE ON UPDATE CASCADE ;

SQL
                       );

    }

    public function down()
    {
        echo "m150212_204048_favorites cannot be reverted.\n";

        return false;
    }
}
