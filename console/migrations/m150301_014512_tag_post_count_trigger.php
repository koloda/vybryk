<?php

use yii\db\Schema;
use yii\db\Migration;

class m150301_014512_tag_post_count_trigger extends Migration
{
    public function up()
    {
        $this->execute(<<<SQL

 CREATE TRIGGER insert_tag_to_post BEFORE INSERT ON tag_to_post
 FOR EACH ROW
 BEGIN
    UPDATE tag SET posts_count = posts_count + 1 WHERE id = NEW.tag_id;
 END;

                       
SQL
                       );
    }

    public function down()
    {
        echo "m150301_014512_tag_post_count_trigger cannot be reverted.\n";

        return false;
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
