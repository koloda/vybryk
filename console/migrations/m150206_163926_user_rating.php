<?php

use yii\db\Schema;
use yii\db\Migration;

class m150206_163926_user_rating extends Migration
{
    public function up()
    {
        $this->execute(<<<SQL

ALTER TABLE  `profile` ADD  `rating` INT( 11 ) NOT NULL DEFAULT  '0',
ADD  `posts_count` INT( 9 ) UNSIGNED NOT NULL DEFAULT  '0',
ADD  `comments_count` INT( 9 ) UNSIGNED NOT NULL DEFAULT  '0';

SQL
        );

    }

    public function down()
    {
        echo "m150206_163926_user_rating cannot be reverted.\n";

        return false;
    }
}
