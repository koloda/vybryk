<?php

use yii\db\Schema;
use yii\db\Migration;

class m150221_002006_comment_triggers extends Migration
{
    public function up()
    {
        $this->execute(<<<SQL


 CREATE TRIGGER insert_comment BEFORE INSERT ON comment
 FOR EACH ROW
 BEGIN
     UPDATE post SET comments_count = comments_count + 1 WHERE id = NEW.post_id;
 END;


 CREATE TRIGGER delete_comment BEFORE DELETE ON comment
 FOR EACH ROW
 BEGIN
    UPDATE post SET comments_count = comments_count - 1 WHERE id = OLD.post_id;
 END;

 CREATE TRIGGER insert_comment_plusminus BEFORE INSERT ON comment_plus_minus
 FOR EACH ROW
 BEGIN
     IF NEW.sign = 'plus' THEN
         UPDATE comment SET pluses_count = pluses_count + 1 WHERE id = NEW.comment_id;
     ELSEIF NEW.sign = 'minus' THEN
         UPDATE comment SET minuses_count = minuses_count + 1 WHERE id = NEW.comment_id;
     END IF;
 END;

 CREATE TRIGGER delete_comment_plusminus BEFORE DELETE ON comment_plus_minus
 FOR EACH ROW
 BEGIN
     IF OLD.sign = 'plus' THEN
         UPDATE comment SET pluses_count = pluses_count - 1 WHERE id = OLD.comment_id;
     ELSEIF OLD.sign = 'minus' THEN
         UPDATE comment SET minuses_count = minuses_count - 1 WHERE id = OLD.comment_id;
     END IF;
 END;

SQL
                       );

    }

    public function down()
    {
        echo "m150221_002006_comment_triggers cannot be reverted.\n";

        return false;
    }
}
