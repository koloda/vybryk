<?php

use yii\db\Schema;
use yii\db\Migration;

class m150213_142729_comments extends Migration
{
    public function up()
    {
        $this->execute(<<<SQL

CREATE TABLE IF NOT EXISTS `comment` (
  `id` int(16) NOT NULL AUTO_INCREMENT,
  `post_id` int(16) NOT NULL,
  `user_id` int(16) NOT NULL,
  `parent_id` int(16) DEFAULT NULL,
  `text` text NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `pluses_count` int(6) DEFAULT '0',
  `minuses_count` int(10) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `post_id` (`post_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

ALTER TABLE `comment` ADD COLUMN `original_parent_id` Int( 16 ) NULL;

ALTER TABLE  `comment` ADD FOREIGN KEY (  `post_id` ) REFERENCES  `post` (
`id`
) ON DELETE CASCADE ON UPDATE CASCADE ;

ALTER TABLE  `comment` ADD FOREIGN KEY (  `user_id` ) REFERENCES  `user` (
`id`
) ON DELETE CASCADE ON UPDATE CASCADE ;

SQL
                       );

    }

    public function down()
    {
        echo "m150213_142729_comments cannot be reverted.\n";

        return false;
    }
}
