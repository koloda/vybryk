/**
 * Post likes, dislikes, adding to favorites
 */
 
$(function(){

    $('.col-xs-9.content').on('click', '.post_down_icon, .post_up_icon', function(){
        var btn = $(this);
        
        if(btn.hasClass('disabled'))
            return false;

        $.get(btn.data('url'), function(data){
            if(data.success)
            {
                btn.closest('.down_post').find('.plus-counter').text('+' + data.pluses);
                btn.closest('.down_post').find('.minus-counter').text('-' + data.minuses);

                btn.closest('.down_post').find('.post_down_icon, .post_up_icon').removeClass('disabled');
                btn.addClass('disabled');
            }
        }, 
        'json'
        );
    });


    $('.col-xs-9.content').on('click', '.add_post_icon', function(){
        var btn = $(this);

        if(btn.hasClass('disabled'))
            return false;

        $.get(btn.data('url'), function(data){
            if(data.success)
            {
                btn.addClass('disabled');
            }
        }, 
        'json'
        );
    });

});