$(document).ready(function() {

    $('.tooltip_text').tooltip({
        animation: true,
        delay: 100
    });

    /*Custom select*/
    $('.selectpicker').selectpicker();

    $('.up_arrow').click(function() {
        var str = $(this).attr('href');
        $.scrollTo(str, 1000, {offset: -100});
        return false;
    });

    $('.add_post_icon img').click(function() {
        $(this).addClass("animate_add_post");
    });

    /*$('.form-control').focusout(function() {
     $(this).addClass('valid');
     });*/

    /*$('.form_main_data .form_email').focus(function () {
     $(this).addClass('valid');
     });*/


    /*Close block*/
    $('.close_tags').on("click", function() {
        $(this).parents('.tags').fadeOut();
    });

    $('.close_block_').on("click", function() {
        $(this).parents('.block_user').fadeOut();
    });
    $('.close_block_').on("click", function() {
        $(this).parents('.block_comment').fadeOut();
    });
    $('.advice .close_advice').click(function() {
        $('.advice').slideUp("slow");
        $('.advice + .line_1').slideUp("slow");
    });
    $('.btn_unsubscribe').on("click", function() {
        $(this).parents('.subscriber').fadeOut();
    });

    /*Show/hide comment*/
    $('.show_comment').click(function() {
        //$(this).parent().parent().next('.answer_comment').slideToggle();
        $(this).parent().parent().parent().find('.answer_comment').slideToggle();
        if ($(this).text() === "Показати") {
            $(this).text("Сховати");
            $(this).css('background-image', 'url(/img/arrow_hide_comment.png)');
        }
        else
        {
            $(this).text("Показати");
            $(this).css('background-image', 'url(/img/arrow_show_comment.png)');
        }
    });

    /*$('.show_comment').mouseenter(function(){
        if ($(this).text() === "Сховати") {
            $(this).css('background-image', 'url(img/arrow_show_comment_hover.png)');
        }
         else
        {
            $(this).css('background-image', 'url(img/arrow_hide_comment_hover.png)');
        }
    });
    $('.show_comment').mouseout(function(){
        if ($(this).text() === "Сховати") {
            $(this).css('background-image', 'url(img/arrow_show_comment.png)');
        }
        else
        {
            $(this).css('background-image', 'url(img/arrow_hide_comment.png)');
        }
    });*/
    // $('.reply_comment').click(function() {
    //     var author = $(this).data('author');
    //     var parent_id = $(this).data('parentid');

    //     console.log(parent_id);

    //     $('.field-comment-parent_id input').val(parseInt(parent_id));
    //     $('.field-comment-original_parent_id input').val(parseInt(parent_id));
    //     $('.block_add_comment textarea.form-control').focus();
    //     $('.block_add_comment textarea.form-control').val(author + ', ');
    // });

    /*Custom range*/
    /*$("#ex6, #ex7, #ex8").slider();
     $("#ex6").on('slide', function(slideEvt) {
     $("#ex6SliderVal6").text(slideEvt.value);
     });
     $("#ex7").on('slide', function(slideEvt) {
     $("#ex6SliderVal7").text(slideEvt.value);
     });
     $("#ex8").on('slide', function(slideEvt) {
     $("#ex6SliderVal8").text(slideEvt.value);
     });*/

    /*Клік показати/сховати*/
    $(".sidebar_login_reg .change_pass").click(function() {
        $(this).toggleClass("transparent");
    });

    $(".security_change_pass").click(function() {
        $(this).find("img.bottom").toggleClass("show_pass");
    });



    $('.content').on('click', '[data-toggle="tabajax"]', function(e) {
        var $this = $(this),
            loadurl = $this.attr('data-href'),
            targ = $this.attr('data-target');

        $.get(loadurl, function(data) {
            $(targ).html(data);

            offset = 10;
        });

        $this.tab('show');
        return false;
    });

    $('.content').on('click', '.sort_post .filter', function(){
        $('.sort_post .filter').removeClass('active');
        $(this).addClass('active');
    });

    // $('[data-toggle="tabajax"].main-tab').on('click', function(){
    //     $(this).tab('show');
    //     return false;
    // })

    //@TODO: fix this later
    //$('.your_note textarea, .custom_tab textarea, .add_comment textarea').autosize();
});

/*Стрілка up_arrow*/
/*$(window).bind('scroll', function() {
 if ($(window).scrollTop() > 1000) {
 $('.up_arrow').fadeIn().addClass("animate");
 $('.up_arrow').removeClass("animate_up");
 }
 else {
 $('.up_arrow').removeClass("animate");
 $('.up_arrow').fadeOut().addClass("animate_up");
 }
 });*/

$(window).bind('scroll', function() {
    if ($(window).scrollTop() > 1000) {
        $('.up_arrow').fadeIn().addClass("animate");
    }
    else {
        $('.up_arrow').fadeOut();
    }
});

/*Поява add_comment*/
$(window).scroll(function() {
    var clientHeight = document.documentElement.clientHeight;
    var height_post = $('.body_post').height();
    var height = $('body').height();
    var new_h = $(window).scrollTop() + clientHeight;
    if ($(window).scrollTop() > height_post - 200) {
        $('.block_add_comment').fadeIn();
        /*var block_comment_post_page = $('.block_comment_post_page').css('height').split('px');
         var block_add_comment_height = $('.block_add_comment').css('height').split('px');
         var block_add_comment_position = $('.block_add_comment').position().top;  */
    }
    else {
        $('.block_add_comment').fadeOut();
    }

    if (new_h == height) {
        $('.block_add_comment').css({"bottom": "60px"})
    }
    else {
        $('.block_add_comment').css({"bottom": "0px"})
    }
    //alert(block_add_comment_position);
    /*if((block_comment_post_page[0] -500) === (block_add_comment_height[0] + block_add_comment_position)){
     alert("dfuyhf");
     $('.block_add_comment').css('bottom', '80px');
     }*/
});

/*Зміна input type*/
function pass() {
    if (document.getElementById('our_pass').getAttribute('type') === 'password') {
        document.getElementById('our_pass').type = 'text';
    }
    else if (document.getElementById('our_pass').getAttribute('type') === 'text') {
        document.getElementById('our_pass').type = 'password';
    }
}

function pass1() {
    if (document.getElementById('our_pass1').getAttribute('type') === 'password') {
        document.getElementById('our_pass1').type = 'text';
    }
    else if (document.getElementById('our_pass1').getAttribute('type') === 'text') {
        document.getElementById('our_pass1').type = 'password';
    }
}

function pass2() {
    if (document.getElementById('our_pass2').getAttribute('type') === 'password') {
        document.getElementById('our_pass2').type = 'text';
    }
    else if (document.getElementById('our_pass2').getAttribute('type') === 'text') {
        document.getElementById('our_pass2').type = 'password';
    }
}
function pass3() {
    if (document.getElementById('our_pass3').getAttribute('type') === 'password') {
        document.getElementById('our_pass3').type = 'text';
    }
    else if (document.getElementById('our_pass3').getAttribute('type') === 'text') {
        document.getElementById('our_pass3').type = 'password';
    }
}
function pass4() {
    if (document.getElementById('our_pass3').getAttribute('type') === 'password') {
        document.getElementById('our_pass3').type = 'text';
    }
    else if (document.getElementById('our_pass3').getAttribute('type') === 'text') {
        document.getElementById('our_pass3').type = 'password';
    }
}

/**
 * parse url and return all GET-params as object
 * copied from http://dontforget.pro/
 */
function getUrlVars()
{
    var vars = {}, hash;
    var hashes = window.location.toString().slice(window.location.href.indexOf('?') + 1).split('&');

    for(var i = 0; i < hashes.length; i++)
    {
        hash = hashes[i].split('=');
        vars[hash[0]] = hash[1];
    }
    
    return vars;
}


