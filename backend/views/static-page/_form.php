<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use wbraganca\dynamicform\DynamicFormWidget;

/* @var $this yii\web\View */
/* @var $model frontend\models\StaticPage */
/* @var $form yii\widgets\ActiveForm */
/* @var $tags array|frontend\models\StaticPageMetaTag */
?>

<div class="static-page-form">

    <?php $form = ActiveForm::begin(['id' => 'static-page-form']); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => 255]) ?>

    <div class="form-group">
        <?= Html::activeLabel($model, 'content') ?>
        <?=
        yii\imperavi\Widget::widget([
            'model' => $model,
            'attribute' => 'content',
            'options' => [
                'imageEditable' => false,
                'minHeight' => 300,
                'maxHeight' => 300,
                'convertImagesLinks' => true,
                'convertVideoLinks' => true,
                'linebreaks' => true,
            ],
            'plugins' => [
                'fontcolor'
            ],
        ]);
        ?>
    </div>

    <div class="panel panel-default">
        <div class="panel-heading"><h4><i class="glyphicon glyphicon-pushpin"></i> Мета теги</h4></div>
        <div class="panel-body">
            <?php
            DynamicFormWidget::begin([
                'widgetContainer' => 'dynamicform_wrapper',
                'widgetBody' => '.container-items',
                'widgetItem' => '.item',
                'min' => 0, // 0 or 1 (default 1)
                'insertButton' => '.add-item',
                'deleteButton' => '.remove-item',
                'model' => $tags[0],
                'formId' => 'static-page-form',
                'formFields' => [
                    'name',
                    'meta'
                ],
            ]);
            ?>

            <div class="row container bottom-buffer">
                <button type="button" class="add-item btn btn-success btn-md"><i class="glyphicon glyphicon-plus"></i> Додати</button>
            </div>


            <div class="container-items">
                <?php foreach ($tags as $i => $tag): ?>
                    <div class="item panel panel-default">
                        <div class="panel-heading">
                            <div class="pull-right">
                                <button type="button" class="remove-item btn btn-danger btn-xs"><i class="glyphicon glyphicon-minus"></i></button>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="panel-body">
                            <?php
                            if (!$tag->isNewRecord) {
                                echo Html::activeHiddenInput($tag, "[{$i}]id");
                            }
                            ?>
                            <div class="form-group">
                                <?= $form->field($tag, "[{$i}]name")->textInput(['maxlength' => 255]) ?>
                            </div>
                            <div class="form-group">
                                <?= $form->field($tag, "[{$i}]meta")->textInput(['maxlength' => 255]) ?>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
            <?php DynamicFormWidget::end(); ?>
        </div>
    </div>




    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
