<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\StaticPage */
/* @var $tags array|frontend\models\StaticPageMetaTag */

$this->title = 'Створення сторінки';
$this->params['breadcrumbs'][] = ['label' => 'Сторінки', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="static-page-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'tags' => $tags
    ]) ?>

</div>
