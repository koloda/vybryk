<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\StaticPage */
/* @var $tags array|frontend\models\StaticPageMetaTag */

$this->title = 'Рудагування сторінки: ' . ' ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Сторінки', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редагування';
?>
<div class="static-page-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'tags' => $tags,
    ]) ?>

</div>
