<?php

namespace backend\controllers;

use Yii;
use frontend\models\StaticPage;
use frontend\models\StaticPageMetaTag;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\models\DynamicModel;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\web\Response;

/**
 * StaticPageController implements the CRUD actions for StaticPage model.
 */
class StaticPageController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all StaticPage models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => StaticPage::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single StaticPage model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new StaticPage model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new StaticPage();
        $tags = [new StaticPageMetaTag];

        if ($model->load(Yii::$app->request->post())) {

            $tags = DynamicModel::createMultiple(StaticPageMetaTag::classname());
            DynamicModel::loadMultiple($tags, Yii::$app->request->post());


            if (Yii::$app->request->isAjax) {
                return $this->ajaxValidateWithTags($model, $tags);
            }

            if ($model->validate()) {
                $transaction = \Yii::$app->db->beginTransaction();
                try {
                    if ($flag = $model->save(false)) {
                        foreach ($tags as $tag) {
                            $tag->static_page_id = $model->id;
                            if (!($flag = $tag->save())) {
                                $transaction->rollBack();
                                break;
                            }
                        }
                    }
                    if ($flag) {
                        $transaction->commit();
                        return $this->redirect(['view', 'id' => $model->id]);
                    }
                } catch (\Exception $e) {
                    $transaction->rollBack();
                }
            }
        }

        return $this->render('create', [
            'model' => $model,
            'tags' => (empty($tags)) ? [new StaticPageMetaTag] : $tags
        ]);
    }

    /**
     * Updates an existing StaticPage model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $tags = $model->staticPageMetaTags;

        if ($model->load(Yii::$app->request->post())) {

            $oldIDs = ArrayHelper::map($tags, 'id', 'id');
            $tags = DynamicModel::createMultiple(StaticPageMetaTag::classname(), $tags);
            DynamicModel::loadMultiple($tags, Yii::$app->request->post());
            $deletedIDs = array_diff_key($oldIDs, array_filter(ArrayHelper::map($tags, 'id', 'id')));

            if (Yii::$app->request->isAjax) {
                return $this->ajaxValidateWithTags($model, $tags);
            }

            if ($model->validate()) {
                $transaction = \Yii::$app->db->beginTransaction();
                try {
                    if ($flag = $model->save(false)) {
                        if (! empty($deletedIDs)) {
                            StaticPageMetaTag::deleteAll(['id' => $deletedIDs]);
                        }
                        foreach ($tags as $tag) {
                            $tag->static_page_id = $model->id;
                            if (! ($flag = $tag->save())) {
                                $transaction->rollBack();
                                break;
                            }
                        }
                    }
                    if ($flag) {
                        $transaction->commit();
                        return $this->redirect(['view', 'id' => $model->id]);
                    }
                } catch (\Exception $e) {
                    $transaction->rollBack();
                }
            }
        }

        return $this->render('update', [
            'model' => $model,
            'tags' => (empty($tags)) ? [new StaticPageMetaTag] : $tags
        ]);
    }

    /**
     * Deletes an existing StaticPage model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the StaticPage model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return StaticPage the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = StaticPage::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function ajaxValidateWithTags($model, $tags)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        return ArrayHelper::merge(
            ActiveForm::validateMultiple($tags),
            ActiveForm::validate($model)
        );
    }
}
