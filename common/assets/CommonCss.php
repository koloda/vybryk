<?php

namespace common\assets;

use yii\web\AssetBundle;

class CommonCss extends AssetBundle
{
    public $sourcePath = '@veryRoot/css';
    public $css = [
        'style.css'
    ];
    public $js = [
    ];
}