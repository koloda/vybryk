<?php

Yii::setAlias('@veryRoot', realpath(dirname(__FILE__).'/../../'));

return [
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'urlManager' => [
                'class' => 'yii\web\UrlManager',
                'enablePrettyUrl' => true,
                'showScriptName' => false,
                // 'rules' => [
                //     'tag/<id:\d+>' => 'tag/index',
                //     'tag/load/<id:\d+>' => 'tag/load',
                // ]
        ],
        'view' => [
            'theme' => [
                'pathMap' => [
                    '@dektrium/user/views' => '@app/views/user',
                    '@dektrium/user/views/layouts' => '@app/views/layouts'
                ],
            ],
        ],
        'authClientCollection' => [
            'class' => 'yii\authclient\Collection',
            'clients' => [
                // 'google' => [
                //     'class' => 'yii\authclient\clients\GoogleOpenId'
                // ],

                'facebook' => [
                    'class' => 'yii\authclient\clients\Facebook',
                    'clientId' => 'facebook_client_id',
                    'clientSecret' => 'facebook_client_secret',
                ],
                'vkontakte' => [
                    'class' => 'yii\authclient\clients\VKontakte',
                    'clientId' => 'vk_client_id',
                    'clientSecret' => 'vk_client_secret',
                ],
                'twitter' => [
                    'class' => 'yii\authclient\clients\Twitter',
                    'consumerKey' => 'zPpF0Amc6awjXGWKcnF11gpHJ',
                    'consumerSecret' => 'mOXTCowNLbQ1SCxE8wGZMj29a5COXB7SXJzDms9RVpKNmr1bK2',
                ],
            ],
        ],
    ],
    'modules'   => [
        'user' => [
            'class' => 'dektrium\user\Module',
            'modelMap'  => [
                'User'  => 'frontend\models\User'
            ]
        ],
    ]
];
