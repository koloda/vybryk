<?php

namespace frontend\controllers;

use frontend\models\Tag;
use frontend\models\Post;
use frontend\models\TagSubscription;
use frontend\models\TagIgnore;
use yii\web\NotFoundHttpException;

class TagController extends \yii\web\Controller
{
    /**
     * Show page with posts, ralated to tag
     * @param  int $id Tag::$id
     * @return View    Tag posts page
     */
    public function actionIndex($id)
    {
        $tag = Tag::find()
            ->where(['id' => $id])
            ->one();

        if(!$tag)
            throw new NotFoundHttpException(\Yii::t('app', 'Tag not found - exception message'), 404);

        \Yii::$app->view->registerJs('var loadURL = "/tag/load/";', 1);

        $posts = $this->getRecords($tag->id);

        return $this->render('index', ['tag' => $tag, 'posts' => $posts]);
    }

    /**
     * AJAX load tag-page posts
     * @param  int  $id    Tag::$id
     * @param  int $offset Posts offset
     * @return View        HTML vith posts  
     */
    public function actionLoad($id, $offset = 0)
    {
        $posts = $this->getRecords($id, (int)$offset);
        return $this->renderPartial('_load', ['posts' => $posts]);
    }

    /**
     * Returns posts related to tag
     * @param  int  $tag_id Tag::$id
     * @param  int $offset Offset
     * @return Post[]
     */
    private function getRecords($tag_id, $offset = 0)
    {
        $posts = Post::find()
            ->join('INNER JOIN', 'tag_to_post', 'tag_to_post.post_id = post.id AND tag_to_post.tag_id = '. $tag_id)
            ->limit(POSTS_PER_PAGE)
            ->with('postTags')
            ->with('user')
            ->offset((int)$offset)
            ->all();

        return $posts;
    }

    /**
     * Subscriptions and ignore-lists
     */

    /**
     * Ignore
     */

    /**
     * Add tag to ignore-list
     * @param  int $id Tag::$id
     * @return JSON    {success:<bool>}
     */
    public function actionIgnore($id)
    {
        $response = ['success' => true];

        $subscription = TagSubscription::find()
            ->where(['tag_id' => (int)$id, 'user_id' => \Yii::$app->user->id])
            ->one();

        if($subscription)
            $subscription->delete();

        $ignore = TagIgnore::find()
            ->where(['tag_id' => (int)$id, 'user_id' => \Yii::$app->user->id])
            ->one();

        if(!$ignore)
        {
            $ignore = new TagIgnore;
            $ignore->tag_id = (int)$id;
            $ignore->user_id = \Yii::$app->user->id;

            if($ignore->validate())
                try 
                {
                    $ignore->save();
                    $response['success'] = true;
                }
                catch (Exception $e)
                {}
        }

        echo json_encode($response);
    }

    /**
     * Remove tag from ignore-list
     * @param  int $id Tag::$id
     * @return JSON    {success:<bool>}
     */
    public function actionUnignore($id)
    {
        $response = ['success' => true];

        $ignore = TagIgnore::find()
            ->where(['tag_id' => (int)$id, 'user_id' => \Yii::$app->user->id])
            ->one();

        if($ignore)
            $ignore->delete();

        echo json_encode($response);
    }

    /**
     * Subscribe
     */

    /**
     * Add tag to subscriptions
     * @param  int $id Tag::$id
     * @return JSON    {success:<bool>}
     */
    public function actionSubscribe($id)
    {
        $response = ['success' => false];

        $ignore = TagIgnore::find()
            ->where(['tag_id' => (int)$id, 'user_id' => \Yii::$app->user->id])
            ->one();

        if($ignore)
            $ignore->delete();

        $subscription = TagSubscription::find()
            ->where(['tag_id' => (int)$id, 'user_id' => \Yii::$app->user->id])
            ->one();

        if(!$subscription)
        {
            $subscription = new TagSubscription;
            $subscription->tag_id = (int)$id;
            $subscription->user_id = \Yii::$app->user->id;

            if($subscription->validate())
                try 
                {
                    $subscription->save();
                    $response['success'] = true;
                }
                catch (Exception $e)
                {}
        }

        echo json_encode($response);
    }

    /**
     * Remove tag from subscriptions
     * @param  int $id Tag::$id
     * @return JSON    {success:<bool>}
     */
    public function actionUnsubscribe($id)
    {
        $response = ['success' => true];

        $subscription = TagSubscription::find()
            ->where(['tag_id' => (int)$id, 'user_id' => \Yii::$app->user->id])
            ->one();

        if($subscription)
            $subscription->delete();

        echo json_encode($response);
    }

}
