<?php
namespace frontend\controllers;

use frontend\models\Post;
use frontend\models\User;

class FreshController extends BaseController
{
    public function actionIndex()
    {
        \Yii::$app->view->registerJs('var loadURL = "/fresh/load";', 1);

        $posts = Post::find()
            ->orderBy('created DESC')
            ->limit(POSTS_PER_PAGE)
            ->with('postTags')
            ->with('user')
            ->all();

        return $this->render('index', ['posts' => $posts]);
    }

    public function actionLoad($offset)
    {
        $posts = Post::find()
            ->orderBy('created DESC')
            ->offset((int)$offset)
            ->limit(POSTS_PER_PAGE)
            ->with('postTags')
            ->with('user')
            ->all();

        return $this->renderPartial('_load', ['posts' => $posts]);   
    }

}
