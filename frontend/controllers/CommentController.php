<?php

namespace frontend\controllers;

use frontend\models\Comment;
use yii\web\Response;
use yii\widgets\ActiveForm;
use frontend\models\FavoriteComments;
use frontend\models\CommentPlusMinus;

class CommentController extends \yii\web\Controller
{
    public $layout = 'empty';

    public function actionCompose()
    {
        if(\Yii::$app->user->isGuest || !\Yii::$app->user->identity->canAddComment())
            return null;

        $model = new Comment();

        $model->user_id = \Yii::$app->user->id;
        $model->post_id = (int)\Yii::$app->request->get('post_id');

        if( $this->performAjaxValidation($model) )
        {
            if(\Yii::$app->request->post('ajax') == null)
                if ($model->load(\Yii::$app->request->post()))
                {
                    if ($model->validate()) 
                    {
                        $model->save();
                        echo json_encode(['success' => true, 'id'=>$model->id]);
                        \Yii::$app->end();
                    }
                }
        }

        return $this->renderAjax('compose', [
            'model' => $model,
        ]);
    }

    public function actionPlusMinus($comment_id, $sign)
    {
        $response = ['success' => false, 'pluses' => 0, 'minuses' => 0];

        if(in_array($sign, ['plus', 'minus']) && \Yii::$app->user->identity->canRate())
        {
            $plusMinus = CommentPlusMinus::find()
                ->where(['comment_id' => (int)$comment_id, 'user_id' => \Yii::$app->user->id])
                ->one();

            if($plusMinus)
                $plusMinus->delete();

            $plusMinus = new CommentPlusMinus;
            $plusMinus->comment_id = (int)$comment_id;
            $plusMinus->user_id = \Yii::$app->user->id;
            $plusMinus->sign = $sign;

            if($plusMinus->validate())
                try 
                {
                    $plusMinus->save();
                    $response['success'] = true;
                }
                catch (Exception $e)
                {}

            //get comment plus and minus values after update
            $comment = Comment::findOne($comment_id);
            if($comment)
            {
                $response['pluses'] = $comment->pluses_count;
                $response['minuses'] = $comment->minuses_count;
            }
        }

        echo json_encode($response);
    }

    public function actionAddToFavorites($comment_id)
    {
        $response = ['success' => false];

        $favorite = FavoriteComments::find()
            ->where(['comment_id' => (int)$comment_id, 'user_id' => \Yii::$app->user->id])
            ->one();

        if(!$favorite)
        {
            $favorite = new FavoriteComments;
            $favorite->comment_id = (int)$comment_id;
            $favorite->user_id = \Yii::$app->user->id;

            if($favorite->validate())
                try 
                {
                    $favorite->save();
                    $response['success'] = true;
                }
                catch (Exception $e)
                {}
        }

        echo json_encode($response);
    }

    /**
    * Performs ajax validation.
    * @param Model $model
    * @throws \yii\base\ExitException
    */
    protected function performAjaxValidation(Comment $model)
    {
        if (\Yii::$app->request->isAjax && $model->load(\Yii::$app->request->post())) {
            \Yii::$app->response->format = Response::FORMAT_JSON;
            
            $errors = ActiveForm::validate($model);
            if($model->hasErrors()) {
                echo json_encode($errors);
                \Yii::$app->end();
            }

            return true;
        }
    }

}
