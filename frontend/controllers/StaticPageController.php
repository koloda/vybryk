<?php
namespace frontend\controllers;

use frontend\models\StaticPage;
use \yii\web\HttpException;

class StaticPageController extends BaseController
{
    public function actionView($title)
    {
        $model = StaticPage::findOne(['title' => $title]);

        if (!$model) {
            throw new HttpException('404', 'Сторінка не знайдена', 404);
        }

        $this->metaTags = $model->staticPageMetaTags;

        return $this->render('view', ['model' => $model]);
    }
}