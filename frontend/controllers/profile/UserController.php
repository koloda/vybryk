<?php

namespace frontend\controllers\profile;

use frontend\models\User;
use frontend\models\UserIgnore;
use frontend\models\UserSubscription;

class UserController extends \yii\web\Controller
{
    public function actionIndex($id)
    {
        $user = User::findOne($id);

        if (!$user)
            throw new NotFoundHttpException;

        \Yii::$app->view->registerJs('var loadURL = "/profile/user/load";', 1);

        echo $this->render('index', ['user' => $user]);
    }

    public function actionLoad($offset=0, $id)
    {
        $user = User::findOne($id);

        if (!$user)
            throw new NotFoundHttpException;

        \Yii::$app->view->registerJs('var loadURL = "/profile/user/load";', 1);

        $posts = $user->getPosts()
            ->orderBy('created DESC')
            ->offset((int)$offset)
            ->limit(POSTS_PER_PAGE)
            ->all();

        return $this->renderPartial('_posts', ['posts' => $posts]);
    }

    /**
     * Add user to ignore-list
     * @param  int $id User::$id
     * @return JSON    {success:<bool>}
     */
    public function actionIgnore($id)
    {
        $response = ['success' => true];

        $subscription = UserSubscription::find()
            ->where(['user_id' => (int)$id, 'follower_id' => \Yii::$app->user->id])
            ->one();

        if($subscription)
            $subscription->delete();

        $ignore = UserIgnore::find()
            ->where(['ignored_id' => (int)$id, 'user_id' => \Yii::$app->user->id])
            ->one();

        if(!$ignore)
        {
            $ignore = new UserIgnore;
            $ignore->ignored_id = (int)$id;
            $ignore->user_id = \Yii::$app->user->id;

            if($ignore->validate())
                try 
                {
                    $ignore->save();
                    $response['success'] = true;
                }
                catch (Exception $e)
                {}
        }

        echo json_encode($response);
    }

    /**
     * Remove user from ignore-list
     * @param  int $id User::$id
     * @return JSON    {success:<bool>}
     */
    public function actionUnignore($id)
    {
        $response = ['success' => true];

        $ignore = UserIgnore::find()
            ->where(['ignored_id' => (int)$id, 'user_id' => \Yii::$app->user->id])
            ->one();

        if($ignore)
            $ignore->delete();

        echo json_encode($response);
    }

    /**
     * Subscribe
     */

    /**
     * Add user to subscriptions
     * @param  int $id User::$id
     * @return JSON    {success:<bool>}
     */
    public function actionSubscribe($id)
    {
        $response = ['success' => false];

        $ignore = UserIgnore::find()
            ->where(['ignored_id' => (int)$id, 'user_id' => \Yii::$app->user->id])
            ->one();

        if($ignore)
            $ignore->delete();

        $subscription = UserSubscription::find()
            ->where(['user_id' => (int)$id, 'follower_id' => \Yii::$app->user->id])
            ->one();

        if(!$subscription)
        {
            $subscription = new UserSubscription;
            $subscription->user_id = (int)$id;
            $subscription->follower_id = \Yii::$app->user->id;

            if($subscription->validate())
                try 
                {
                    $subscription->save();
                    $response['success'] = true;
                }
                catch (Exception $e)
                {}
        }

        echo json_encode($response);
    }

    /**
     * Remove user from subscriptions
     * @param  int $id User::$id
     * @return JSON    {success:<bool>}
     */
    public function actionUnsubscribe($id)
    {
        $response = ['success' => true];

        $subscription = UserSubscription::find()
            ->where(['user_id' => (int)$id, 'follower_id' => \Yii::$app->user->id])
            ->one();

        if($subscription)
            $subscription->delete();

        echo json_encode($response);
    }


}
