<?php

namespace frontend\controllers\profile;

class WallController extends \yii\web\Controller
{
    public function actionIndex()
    {
        \Yii::$app->view->registerJs('var loadURL = "/profile/wall/likes";', 1);
        \Yii::$app->view->registerJs('var loadContentTarget = "#tab1";', 1);

        $posts = \Yii::$app->user->identity->getRatedPosts()
            ->where("plus_minus.sign = 'plus'")
            ->limit(POSTS_PER_PAGE)
            ->all();

        $panel = $this->renderPartial('_panel');

        return $this->render('index', ['posts' => $posts, 'panel' => $panel]);
    }

    public function actionLikes($offset=0)
    {
        \Yii::$app->view->registerJs('var loadURL = "/profile/wall/likes";', 1);
        \Yii::$app->view->registerJs('var loadContentTarget = "#tab1";', 1);

        $posts = \Yii::$app->user->identity->getRatedPosts()
            ->where("plus_minus.sign = 'plus'")
            ->limit(POSTS_PER_PAGE)
            ->offset((int)$offset)
            ->all();

        $panel = null;
        if(!$offset)
            $panel = $this->renderPartial('_panel');

        return $this->renderAjax('posts', ['posts' => $posts, 'panel' => $panel]);
    }

    public function actionDislikes($offset=0)
    {
        \Yii::$app->view->registerJs('var loadURL = "/profile/wall/dislikes";', 1);
        \Yii::$app->view->registerJs('var loadContentTarget = "#tab1";', 1);

        $posts = \Yii::$app->user->identity->getRatedPosts()
            ->where("plus_minus.sign = 'minus'")
            ->limit(POSTS_PER_PAGE)
            ->offset((int)$offset)
            ->all();

        $panel = null;
        if(!$offset)
            $panel = $this->renderPartial('_panel');

        return $this->renderAjax('posts', ['posts' => $posts, 'panel' => $panel]);
    }

    public function actionSubscriptions()
    {
        return $this->render('subscriptions');
    }

    public function actionMy($offset=0)
    {
        \Yii::$app->view->registerJs('var loadURL = "/profile/wall/my";', 1);
        \Yii::$app->view->registerJs('var loadContentTarget = "#tab2";', 1);

        $posts = \Yii::$app->user->identity->getPosts()
            ->limit(POSTS_PER_PAGE)
            ->offset($offset)
            ->all();

        $panel = null;
        if(!$offset)
            $panel = $this->renderPartial('_my_panel');

        return $this->renderAjax('posts', ['posts' => $posts, 'panel' => $panel]);
    }

}
