<?php

namespace frontend\controllers\profile;

class SubscriptionsController extends \yii\web\Controller
{
    public function actionIndex()
    {
    	$users = \Yii::$app->user->identity->userSubscriptions;
    	$tags = \Yii::$app->user->identity->getTagSubscriptions()
    		->select('*, UPPER(LEFT(`text`, 1 )) AS `ch`')
    		->orderBy('ch, text')
    		// ->groupBy('ch')
    		->all();

        return $this->render('index', ['users'=>$users, 'tags'=>$tags]);
    }

    public function actionTags()
    {
        return $this->render('tags');
    }

}
