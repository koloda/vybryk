<?php

namespace frontend\controllers\profile;

class SavedController extends \yii\web\Controller
{

    public function actionIndex()
    {
        \Yii::$app->view->registerJs('var loadURL = "/profile/saved/posts";', 1);

        $posts = \Yii::$app->user->identity->getFavorites()
            ->limit(POSTS_PER_PAGE)
            ->all();

        $panel = $this->renderPartial('_panel');

        return $this->render('index', ['posts'=>$posts, 'panel'=>$panel]);
    }

    public function actionPosts($offset=0)
    {
        \Yii::$app->view->registerJs('var loadURL = "/profile/saved/posts";', 1);
        \Yii::$app->view->registerJs('$("#tab1").removeClass("tab_body").removeClass("tab_body_pad")');

        $posts = \Yii::$app->user->identity->getFavorites()
            ->offset((int)$offset)
            ->limit(POSTS_PER_PAGE)
            ->all();
        
        $panel = '';
        if(!$offset)
            $panel = $this->renderPartial('_panel');

        return $this->renderAjax('_posts', ['posts'=>$posts, 'panel'=>$panel]);
    }

    public function actionComments($offset=0)
    {
        \Yii::$app->view->registerJs('var loadURL = "/profile/saved/comments";', 1);
        \Yii::$app->view->registerJs('$("#tab1").addClass("tab_body tab_body_pad")');

        $comments = \Yii::$app->user->identity->getFavoriteComments()
            ->offset((int)$offset)
            ->limit(POSTS_PER_PAGE)
            ->all();
    
        if(!$offset)
        {
            $panel = $this->renderPartial('_panel');
            return $this->renderAjax('_comments', ['comments'=>$comments, 'panel'=>$panel]);
        }
        else
            return $this->renderAjax('_comments_clean', ['comments'=>$comments]);

    }

}
