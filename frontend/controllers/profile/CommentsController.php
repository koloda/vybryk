<?php

namespace frontend\controllers\profile;

use frontend\models\Comment;
use frontend\models\Post;

class CommentsController extends \yii\web\Controller
{
    public function actionIndex()
    {
        \Yii::$app->view->registerJs('var loadURL = "/profile/comments/my";', 1);
        \Yii::$app->view->registerJs('var loadContentTarget = "#tab1";', 1);

        $comments = \Yii::$app->user->identity->getComments()
            ->limit(POSTS_PER_PAGE)
            ->all();

        return $this->render('index', ['comments' => $comments]);
    }

    public function actionMy($offset=0)
    {
        \Yii::$app->view->registerJs('var loadURL = "/profile/comments/my";', 1);
        \Yii::$app->view->registerJs('var loadContentTarget = "#tab1";', 1);

        $comments = \Yii::$app->user->identity->getComments()
            ->limit(POSTS_PER_PAGE)
            ->offset($offset)
            ->all();

        $panel = '';
        if(!$offset)
        {
            $panel = $this->renderPartial('_panel');
            return $this->renderAjax('comments', ['comments' => $comments, 'panel'=>$panel]);
        }
        else
            return $this->renderAjax('comments_clean', ['comments' => $comments]);   
    }

    public function actionCommentAnsvers($offset=0)
    {
        \Yii::$app->view->registerJs('var loadURL = "/profile/comments/comment-ansvers";', 1);
        \Yii::$app->view->registerJs('var loadContentTarget = "#tab1";', 1);

        $uid = \Yii::$app->user->id;

        $comments = Comment::find()
            ->select('c2.*')
            ->join('INNER JOIN', '`comment` AS `c2`', "c2.original_parent_id = comment.id AND comment.user_id = $uid")
            ->with('user')
            ->limit(POSTS_PER_PAGE)
            ->offset($offset)
            ->all();

        $panel = '';
        if(!$offset)
        {
            $panel = $this->renderPartial('_panel');
            return $this->renderAjax('comments', ['comments' => $comments, 'panel'=>$panel]);
        }
        else
            return $this->renderAjax('comments_clean', ['comments' => $comments]);   
    }

    public function actionPostAnsvers($offset=0)
    {
        \Yii::$app->view->registerJs('var loadURL = "/profile/comments/post-ansvers";', 1);
        \Yii::$app->view->registerJs('var loadContentTarget = "#tab1";', 1);

        $uid = \Yii::$app->user->id;

        $comments = Comment::find()
            ->join('JOIN', '`post`', "post.id = comment.post_id AND post.user_id = $uid")
            ->with('user')
            ->limit(POSTS_PER_PAGE)
            ->offset($offset)
            ->all();

        $panel = '';
        if(!$offset)
        {
            $panel = $this->renderPartial('_panel');
            return $this->renderAjax('comments', ['comments' => $comments, 'panel'=>$panel]);
        }
        else
            return $this->renderAjax('comments_clean', ['comments' => $comments]);   
    }

    
    public function actionReferences($offset=0)
    {
        \Yii::$app->view->registerJs('var loadURL = "/profile/comments/references";', 1);
        \Yii::$app->view->registerJs('var loadContentTarget = "#tab1";', 1);

        $match0 = '%@' . \Yii::$app->user->identity->username . ' %';
        $match1 = '%@' . \Yii::$app->user->identity->username . ',%';

        $comments = Comment::find()
            ->where("text LIKE '$match0' OR text LIKE '$match1'")
            ->with('user')
            ->limit(POSTS_PER_PAGE)
            ->offset($offset)
            ->all();

        $panel = '';
        if(!$offset)
        {
            $panel = $this->renderPartial('_panel');
            return $this->renderAjax('comments', ['comments' => $comments, 'panel'=>$panel]);
        }
        else
            return $this->renderAjax('comments_clean', ['comments' => $comments]);   
    }

    public function actionTop50($offset=0)
    {
        \Yii::$app->view->registerJs('var loadURL = "/profile/comments/top50";', 1);
        \Yii::$app->view->registerJs('var loadContentTarget = "#tab2";', 1);

        if ($offset > 40)
            return false;

        $comments = Comment::find()
            ->select('*, (pluses_count - minuses_count) AS rating')
            ->with('user')
            ->orderBy('rating DESC')
            ->limit(POSTS_PER_PAGE)
            ->offset($offset)
            ->all();

        if(!$offset)
            return $this->renderAjax('comments', ['comments' => $comments]);
        else
            return $this->renderAjax('comments_clean', ['comments' => $comments]);   
    }
}
