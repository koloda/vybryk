<?php

namespace frontend\controllers;

use yii\db\Exception;
use frontend\models\PlusMinus;
use frontend\models\Post;
use frontend\models\User;
use frontend\models\Favorites;
use yii\web\NotFoundHttpException;
use yii\filters\AccessControl;

class PostController extends BaseController
{

    /**
     * Behaviors here
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    ['allow' => true, 'actions' => ['plus-minus', 'add-to-favorites'], 'roles' => ['@']],
                    ['allow' => true, 'actions' => ['view', 'comments'], 'roles' => ['?', '@']],
                ]
            ],
        ];
    }

    public function actionPlusMinus($post_id, $sign)
    {
        $response = ['success' => false, 'pluses' => 0, 'minuses' => 0];

        if(in_array($sign, ['plus', 'minus']) && \Yii::$app->user->identity->canRate())
        {
            $plusMinus = PlusMinus::find()
                ->where(['post_id' => (int)$post_id, 'user_id' => \Yii::$app->user->id])
                ->one();

            if($plusMinus)
                $plusMinus->delete();

            $plusMinus = new PlusMinus;
            $plusMinus->post_id = (int)$post_id;
            $plusMinus->user_id = \Yii::$app->user->id;
            $plusMinus->sign = $sign;

            if($plusMinus->validate())
                try 
                {
                    $plusMinus->save();
                    $response['success'] = true;
                }
                catch (Exception $e)
                {}

            //get post plus and minus values after update
            $post = Post::findOne($post_id);
            if($post)
            {
                $response['pluses'] = $post->pluses_count;
                $response['minuses'] = $post->minuses_count;
            }
        }

        echo json_encode($response);
    }

    public function actionAddToFavorites($post_id)
    {
        $response = ['success' => false];

        $favorite = Favorites::find()
            ->where(['post_id' => (int)$post_id, 'user_id' => \Yii::$app->user->id])
            ->one();

        if(!$favorite)
        {
            $favorite = new Favorites;
            $favorite->post_id = (int)$post_id;
            $favorite->user_id = \Yii::$app->user->id;

            if($favorite->validate())
                try 
                {
                    $favorite->save();
                    $response['success'] = true;
                }
                catch (Exception $e)
                {}
        }

        echo json_encode($response);
    }

    public function actionView($id)
    {
        $post = Post::findOne($id);

        if(!$post)
            throw new NotFoundHttpException(\Yii::t('app', 'Post not found - exception message'), 404);

        return $this->render('view', ['post' => $post]);
    }

    public function actionComments($post_id)
    {
        $post = Post::findOne((int)$post_id);

        return $this->renderAjax('comments', ['post' => $post]);
    }
}
