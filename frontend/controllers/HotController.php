<?php
namespace frontend\controllers;

use frontend\models\Post;
use frontend\models\User;

define('MINIMUM_ACTUAL_RATING', 13);

class HotController extends BaseController
{
    /**
     * SHOW POSTS BY DATE
     */

    public function actionIndex()
    {
        \Yii::$app->view->registerJs('var loadURL = "/hot/load";', 1);

        $posts = $this->getRecords();
        return $this->render('index', ['posts' => $posts]);
    }

    public function actionLoad($offset)
    {
        $posts = $this->getRecords((int)$offset);
        return $this->renderPartial('_load', ['posts' => $posts]);   
    }

    private function getRecords($offset=0)
    {
        $posts = Post::find()
            ->where('(pluses_count - minuses_count) >= 13')
            ->orderBy('created DESC')
            ->offset((int)$offset)
            ->limit(POSTS_PER_PAGE)
            ->with('postTags')
            ->with('user')
            ->all();
        
        return $posts;
    }

    /**
     * SHOW POSTS BY ACTUALITY
     */

    public function actionActual()
    {
        \Yii::$app->view->registerJs('var loadURL = "/hot/load-actual";', 1);

        $posts = $this->getActualRecords();
        return $this->render('index', ['posts' => $posts]);
    }

    public function actionLoadActual($offset)
    {
        $posts = $this->getActualRecords((int)$offset);
        return $this->renderPartial('_load', ['posts' => $posts]);
    }

    private function getActualRecords($offset=0)
    {
        $posts = Post::find()
            ->select('*, (pluses_count - minuses_count)/((NOW() - created)/3600) AS actuality')
            ->where('created < DATE_SUB( NOW( ) , INTERVAL 3 HOUR )')
            ->orderBy('actuality DESC')
            ->offset((int)$offset)
            ->limit(POSTS_PER_PAGE)
            ->with('postTags')
            ->with('user')
            ->all();
        
        return $posts;
    }

}
