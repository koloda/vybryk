<?php
namespace frontend\controllers;

use frontend\models\Post;
use frontend\models\User;

class DisputeController extends BaseController
{
    /**
     * SHOW POSTS BY 24 HOURS COMMENTS COUNT
     */

    public function actionTopComments()
    {
        \Yii::$app->view->registerJs('var loadURL = "/dispute/load-top-comments";', 1);

        $posts = $this->getRecordsTC();
        return $this->render('index', ['posts' => $posts]);
    }

    public function actionLoadTopComments($offset)
    {
        $posts = $this->getRecordsTC((int)$offset);
        return $this->renderPartial('_load', ['posts' => $posts]);   
    }

    private function getRecordsTC($offset=0)
    {
        $posts = Post::find()
            ->select('post.*, COUNT(comment.id) AS cnt')
            ->join('LEFT JOIN', 'comment', 'post.id = comment.post_id')
            ->where(' ( comment.created + INTERVAL 24 HOUR ) > NOW()')
            ->andWhere('( post.created + INTERVAL 2 HOUR ) < NOW()')
            ->orderBy('cnt DESC')
            ->groupBy('post.id')
            ->offset((int)$offset)
            ->limit(POSTS_PER_PAGE)
            ->with('postTags')
            ->with('user')
            ->all();
     
//      var_dump($posts);
//         print_r(count($posts));
// \Yii::$app->end();
        return $posts;
    }

    /**
     * SHOW POSTS BY ACTUALITY
     */

    public function actionActual()
    {
        \Yii::$app->view->registerJs('var loadURL = "/dispute/load-actual";', 1);

        $posts = $this->getActualRecords();
        return $this->render('index', ['posts' => $posts]);
    }

    public function actionLoadActual($offset)
    {
        $posts = $this->getActualRecords((int)$offset);
        return $this->renderPartial('_load', ['posts' => $posts]);
    }

    private function getActualRecords($offset=0)
    {
        $posts = Post::find()
            ->select('*, ((pluses_count - minuses_count)/((NOW() - created)/3600)*comments_count) AS actuality')
            ->where('created < DATE_SUB( NOW( ) , INTERVAL 2 HOUR )')
            ->orderBy('actuality DESC')
            ->offset((int)$offset)
            ->limit(POSTS_PER_PAGE)
            ->with('postTags')
            ->with('user')
            ->all();
        
        return $posts;
    }

}
