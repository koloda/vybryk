<?php
namespace frontend\controllers;

use frontend\models\Post;
use frontend\models\User;

class BestController extends BaseController
{
    /**
     * SHOW POSTS BY RATING
     */

    public function actionIndex()
    {
        \Yii::$app->view->registerJs('var loadURL = "/best/load";', 1);

        $posts = $this->getRecords();
        return $this->render('index', ['posts' => $posts]);
    }

    public function actionLoad($offset)
    {
        $posts = $this->getRecords((int)$offset);
        return $this->renderPartial('_load', ['posts' => $posts]);   
    }

    private function getRecords($offset=0)
    {
        $postsAR = Post::find()
            ->select('*, (pluses_count - minuses_count) AS rating')
            ->orderBy('rating DESC')
            ->offset((int)$offset)
            ->limit(POSTS_PER_PAGE)
            ->with('postTags')
            ->with('user');
        
        return $this->prepareDateFilter($postsAR)->all();
    }

    private function prepareDateFilter($postsAR)
    {
        $intervals = [
            'day'   => '1 DAY',
            'week'  => '1 WEEK',
            'month' => '1 MONTH'
        ];

        $show = \Yii::$app->request->get('show');
        $interval = isset($intervals[$show])?$intervals[$show]:null;

        if($interval)
            $postsAR->where("created > DATE_SUB( NOW(), INTERVAL $interval )");

        return $postsAR;
    }

}
