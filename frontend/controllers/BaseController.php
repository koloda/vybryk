<?php
namespace frontend\controllers;

use \yii\web\Controller;
use frontend\models\StaticPage;
use yii\helpers\Url;

class BaseController extends Controller
{
    protected $upperMenuItems;
    protected $metaTags = [];

    public function init()
    {
        $this->upperMenuItems = $this->createUpperMenuItemsDataArray();
        parent::init();
    }

    public function getUpperMenuItems()
    {
        if (null !== $this->upperMenuItems) {
            return $this->upperMenuItems;
        } else {
            return $this->upperMenuItems = $this->createUpperMenuItemsDataArray();
        }
    }

    protected function createUpperMenuItemsDataArray()
    {
        $models = StaticPage::find()->all();
        $data = [];

        foreach ($models as $model) {
            $data[] = [
                'title' => $model->title,
                'url' => Url::toRoute(['/static-page/view', 'title' => $model->title ])
            ];
        }

        return $data;
    }

    public function getMetaTags()
    {
        return $this->metaTags;
    }
}