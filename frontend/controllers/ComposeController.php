<?php

namespace frontend\controllers;

use yii\base\Model;
use yii\db\Query;
use yii\web\Controller;
use yii\web\Response;
use yii\widgets\ActiveForm;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;

use frontend\models\Post;
use frontend\models\Tag;
use frontend\models\TagToPost;

class ComposeController extends BaseController
{
    public $postingErrors = [];

    /** @inheritdoc */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    ['allow' => true, 'actions' => ['text', 'tags'], 'roles' => ['@']],
                ]
            ],
            // 'verbs' => [
            //     'class' => VerbFilter::className(),
            //     'actions' => [
            //         'tags' => ['ajax']
            //     ]
            // ]
        ];
    }

    public function actionText()
    {
        //@TODO: UNCOMMENT YHIS!
        // if(!\Yii::$app->user->identity->canAddPost('text'))
        // {
        //     return $this->render('error', ['error' => \Yii::$app->user->identity->postAddingError]);
        // }

        $model = new Post(['scenario' => 'create_text']);

        $this->performAjaxValidation($model);

        if ($model->load(\Yii::$app->request->post()))
        {
            if ($model->validate()) 
            {
                $model->user_id = \Yii::$app->user->id;
                $model->careated_date = date('Y-m-d');

                $transaction = \Yii::$app->db->beginTransaction();
                try 
                {
                    $model->save();
                    $this->attachTags($model);
                    $transaction->commit();
                }
                catch (\Exception $e)
                {
                    $transaction->rollBack();
                    throw $e;
                }
                
                $this->redirect(['/post/view', 'id'=>$model->id]);
            }
        }

        return $this->render('create_text', [
            'model' => $model,
        ]);
    }

    public function attachTags($post)
    {
        foreach ($post->tags as $tagText) 
        {
            $tag = Tag::find()->where(['text' => $tagText])->one();
            
            if(!$tag)
            {
                $tag = new Tag();
                $tag->text = $tagText;

                if($tag->validate())
                    $tag->save();
            }

            if($tag && $tag->id)
            {
                //create new relation
                $relation = new TagToPost();
                $relation->tag_id = $tag->id;
                $relation->post_id = $post->id;

                $relation->save();
            }
        }
    }

    public function actionTags($q)
    {
        $out = ['more' => false];

        $q = trim($q);

        if (!is_null($q))
        {
            $query = new Query;

            $query->select("text AS id, CONCAT(`text`, ' (', `posts_count`, ')' ) AS `text` ")
                ->from('tag')
                ->where('text LIKE "%' . $q .'%"')
                ->limit(20);

            $command = $query->createCommand();
            $data = $command->queryAll();

            $out['results'] = array_values($data);

            $match = false;
            foreach ($out['results'] as $tag) 
                if($tag['id'] == $q)
                    $match = true;

            if(!$match)
                $out['results'][] = ['id' => $q, 'text' => $q . ' (0)'];
        }

        echo json_encode($out);
    }

    /**
    * Performs ajax validation.
    * @param Model $model
    * @throws \yii\base\ExitException
    */
    protected function performAjaxValidation(Post $model)
    {
        if (\Yii::$app->request->isAjax && $model->load(\Yii::$app->request->post())) {
            \Yii::$app->response->format = Response::FORMAT_JSON;
            echo json_encode(ActiveForm::validate($model));
            \Yii::$app->end();
        }
    }
}
