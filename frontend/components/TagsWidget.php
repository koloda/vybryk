<?php
namespace app\components;

use frontend\models\Tag;
use yii\base\Widget;
use yii\web\Request;
use \yii\web\HttpException;

class TagsWidget extends Widget
{
    public $tags;
    protected $topTags;
    protected $request;
    protected $days;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function run()
    {
        return $this->render('tags', ['models' => $this->getTopTags(), 'currentDays' => $this->getDays()]);
    }

    public function getTopTags()
    {
        if ($this->topTags !== null) {
            return $this->topTags;
        } else {
            return $this->topTags = Tag::findRecentTop($this->getDays(), true);
        }
    }

    public function getDays()
    {
        if ($this->days !== null) {
            return $this->days;
        } else {
            $days = (int)$this->getRequest()->get('td', 7);

            if (!in_array($days, [7, 14, 24])) {
                throw new HttpException('404', 'Сторінка не знайдена', 404);
            }

            return $this->days = $days;
        }
    }

    /**
     * @return Request instance.
     */
    public function getRequest()
    {
        return $this->request;
    }
}