<?php
use yii\helpers\Html;
use yii\helpers\Url;
?>

<?php
/**
 * @var $models array
 * @var $this yii\web\View
 * @var $currentDays integer
 */
?>

<?php
$this->registerJs(
    "$('#tag-days-picker').on('change', function () { window.location.href = $(this).val() })",
    yii\web\View::POS_END,
    'tag-days-picker'
);
?>

    <div class="title_sidebar_user">
        ТОП тегів
        <div class="select_4">
            <select class="form-control selectpicker" id="tag-days-picker">
                <?php foreach ([7, 14, 24] as $index => $days) : ?>
                    <?=
                    Html::tag(
                        'option',
                        $days . ' ' . ($index === 2 ? 'Доби' : 'Діб'),
                        [
                            'value' => Url::current(['td' => $days])
                        ] + ($days === $currentDays ? ['selected' => 'selected'] : [])
                    )
                    ?>
                <?php endforeach; ?>
            </select>
        </div>
    </div>
    <div class="top_tags">
        <div class="body_top_tags">
            <?php foreach ($models as $model) : ?>
                <p>
                    <?= Html::a('#' . $model->text . ' - ' . $model->posts_count, [''], ['class' => 'tags']) ?>
                </p>
            <?php endforeach; ?>
        </div>
        <a href="" class="link_top_tags_1">О’єднані</a>
        <a href="" class="link_top_tags_2">Всі теги</a>
    </div>

<?php /* echo Url::to(['', 'commentsData' => 5]) */?>