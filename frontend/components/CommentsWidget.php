<?php
namespace app\components;

use yii\base\Widget;
use frontend\models\Comment;

class CommentsWidget extends Widget
{
    public $topComment;

/*    public function init()
    {
        parent::init();

        if ($this->topComment === null) {
            $this->topComment = Comment::findTopComment();
        }
    }*/

    public function run()
    {
        return $this->render('comments', ['topComment' => $this->topComment]);
    }
}