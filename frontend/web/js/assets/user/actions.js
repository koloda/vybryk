/**
 * Subscribe/unsubscribe to User and add/remove User to ignore list
 */
 
$(function()
{
    /**
     * Subscribe/Unsubscribe to user
     */
    $('.col-xs-9.content').on('click', '.user_subscribe, .user_unsubscribe', function(){
        var btn = $(this);
        var surl = '/profile/user/subscribe?id=';
        var usurl = '/profile/user/unsubscribe?id=';
        var id = btn.data('id');

        url = usurl + id;

        if(btn.hasClass('user_subscribe'))            
            url = surl + id;

        $.get(url, function(data)
        {
            if(data.success)
                btn.toggleClass('user_subscribe').toggleClass('user_unsubscribe');

            if(btn.hasClass('user_subscribe'))
                btn.text(btn.data('label'));
            else
                btn.text(btn.data('ulabel'));
        }, 
        'json'
        );
    });

    /**
     * Add/Rm user to ignore-list
     */
    $('.col-xs-9.content').on('click', '.user_ignore, .user_unignore', function(){
        var btn = $(this);
        var surl = '/profile/user/ignore?id=';
        var usurl = '/profile/user/unignore?id=';
        var id = btn.data('id');

        url = usurl + id;

        if(btn.hasClass('user_ignore'))            
            url = surl + id;

        $.get(url, function(data)
        {
            if(data.success)
                btn.toggleClass('user_ignore').toggleClass('user_unignore');

            if(btn.hasClass('user_ignore'))
                btn.text(btn.data('label'));
            else
                btn.text(btn.data('ulabel'));
        }, 
        'json'
        );
    });

});