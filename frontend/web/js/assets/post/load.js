/**
 * AJAX-loading posts into page when scroll
 */

$(function()
{
    loadingInProgress = false;
    canLoad = true;

    $(window).scroll(function(){
        var scrollPos = $(this).scrollTop() + $(window).height();
        var documentHeight = $(document).height();
        if( (documentHeight-300) < scrollPos && !loadingInProgress && canLoad)
            loadPosts();
    });

    offset = 10;
    var perPage = 10;
    var loadContentTarget = '';

    function loadPosts()
    {
        loadingInProgress = true;

        var getVars = getUrlVars();
        getVars['offset'] = offset;

        $.get(loadURL, getVars, function(posts)
        {
            if(posts.length > 1)
            {
                if($('.content ' + loadContentTarget + ' .post').length)
                    $('.content ' + loadContentTarget + ' .post').last().after(posts);
                else
                    $('.content ' + loadContentTarget + ' .block_comment').last().after(posts);

                offset += perPage;
                loadingInProgress = false;
            }
            else
                canLoad = false;
        });
    }
});