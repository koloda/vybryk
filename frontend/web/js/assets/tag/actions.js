/**
 * Subscribe/unsubscribe to Tag and add/remove Tag to ignore list
 */
 
$(function()
{
    /**
     * Subscribe/Unsubscribe to tag
     */
    $('.col-xs-9.content').on('click', '.tag_subscribe, .tag_unsubscribe', function(){
        var btn = $(this);
        var surl = '/tag/subscribe?id=';
        var usurl = '/tag/unsubscribe?id=';
        var id = btn.data('id');

        url = usurl + id;

        if(btn.hasClass('tag_subscribe'))            
            url = surl + id;

        $.get(url, function(data)
        {
            if(data.success)
                btn.toggleClass('tag_subscribe').toggleClass('tag_unsubscribe');

            if(btn.hasClass('tag_subscribe'))
                btn.text(btn.data('label'));
            else
                btn.text(btn.data('ulabel'));
        }, 
        'json'
        );
    });

    /**
     * Add/Rm tag to ignore-list
     */
    $('.col-xs-9.content').on('click', '.tag_ignore, .tag_unignore', function(){
        var btn = $(this);
        var surl = '/tag/ignore?id=';
        var usurl = '/tag/unignore?id=';
        var id = btn.data('id');

        url = usurl + id;

        if(btn.hasClass('tag_ignore'))            
            url = surl + id;

        $.get(url, function(data)
        {
            if(data.success)
                btn.toggleClass('tag_ignore').toggleClass('tag_unignore');

            if(btn.hasClass('tag_ignore'))
                btn.text(btn.data('label'));
            else
                btn.text(btn.data('ulabel'));
        }, 
        'json'
        );
    });

});