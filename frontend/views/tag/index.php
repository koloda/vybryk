<?php
	/* @var $this yii\web\View */

	use yii\web\JqueryAsset;

	$this->title = \Yii::t('app', 'Tag - page title');
	$panel = $this->render('_panel.php', ['tag' => $tag]);
?>

<div class="col-xs-9 content">
    <?php foreach ($posts as $post) 
    {
        $params = ['post' => $post];

        if(isset($panel))
        {
        	$params['panel'] = $panel;
        	unset($panel);
        }

        echo $this->render('../common/post', $params);
    } ?>
</div>

<?php 
    $this->registerJsFile('/js/assets/tag/actions.js', ['depends' => [JqueryAsset::className()]]);
    $this->registerJsFile('/js/assets/post/actions.js', ['depends' => [JqueryAsset::className()]]);
    $this->registerJsFile('/js/assets/post/load.js', ['depends' => [JqueryAsset::className()]]);
?>