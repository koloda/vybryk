<div class="sort_post">
    
    <div class="col-sm-9">
        <h3><?=\Yii::t('app', 'Posts by tag <b>{tag}</b>', ['tag'=>$tag->text])?></h3>
    </div>
    <div class="col-sm-3">
        
        <?php if (!\Yii::$app->user->isGuest): ?>
	
			<?php 
				$class = 'tag_subscribe';
				$label = \Yii::t('app', 'Subscribe');
				if(in_array($tag->id, array_keys(\Yii::$app->user->identity->getUserTagSubscriptions())))
				{
					$class = 'tag_unsubscribe';
					$label = \Yii::t('app', 'Unsubscribe');
				}
			?>
	        <button class="btn <?=$class?>" data-id="<?=$tag->id?>" data-label="<?=\Yii::t('app', 'Subscribe')?>" data-ulabel="<?=\Yii::t('app', 'Unsubscribe')?>">
	        <?=$label?>
	        </button>

			<?php 
				$class = 'tag_ignore';
				$label = \Yii::t('app', 'Ignore');
				if(in_array($tag->id, array_keys(\Yii::$app->user->identity->getUserTagIgnores())))
				{
					$class = 'tag_unignore';
					$label = \Yii::t('app', 'Unignore');
				}
			?>
	        <button class="btn <?=$class?>" data-id="<?=$tag->id?>" data-label="<?=\Yii::t('app', 'Ignore')?>" data-ulabel="<?=\Yii::t('app', 'Unignore')?>">
	        <?=$label?>
	        </button>
        	
        <?php endif ?>

    </div>
</div>