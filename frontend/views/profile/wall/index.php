<?php
/* @var $this yii\web\View */

use yii\web\JqueryAsset;

$this->title = \Yii::t('app', 'Profile wall - page title');
?>


<div class="col-xs-9 content">
    <div role="tabpanel" class="custom_tab">

        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a data-target="#tab1" href="#tab1" data-href="/profile/wall/likes" aria-controls="home" role="tab" data-toggle="tabajax" aria-expanded="true" class="main-tab">Стрічка</a></li>
            <li role="presentation" class=""><a data-target="#tab2" href="#tab2" data-href="/profile/wall/my" aria-controls="profile" role="tab" data-toggle="tabajax" aria-expanded="false">Мої пости</a></li>
        </ul>

        <!-- Tab panes -->
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="tab1">
                
                <?php foreach ($posts as $key => $post): ?>
                    <?=$this->render('../../common/post.php', ['post' => $post, 'panel'=>(!$key)?$panel:null]) ?>
                <?php endforeach ?>

            </div>
            <div role="tabpanel" class="tab-pane" id="tab2">
                
                <!-- POSTS HERE, _PANEL IN FIRST -->

            </div>
        </div>
    </div>
</div>

<?php 
    $this->registerJsFile('/js/assets/post/actions.js', ['depends' => [JqueryAsset::className()]]);
    $this->registerJsFile('/js/assets/post/load.js', ['depends' => [JqueryAsset::className()]]);
?>