<div class="sort_post sort_by">
    <?php 
        $class = '';
        if(in_array($this->context->action->id, ['index', 'likes'])) $class='active'; 
    ?>
    <div class="filter ajaxfilter <?=$class?>" data-target="#tab1" href="#tab1" data-href="/profile/wall/likes" aria-controls="home" role="tab" data-toggle="tabajax"><?=Yii::t('app', 'I like(wall)');?></div>

    <?php $class = '';
        if($this->context->action->id == 'dislikes') $class='active';
    ?>
    <div class="filter ajaxfilter <?=$class?>" data-target="#tab1" href="#tab1" data-href="/profile/wall/dislikes" aria-controls="home" role="tab" data-toggle="tabajax"><?=Yii::t('app', 'I do not like(wall)', array());?></div>

    <div class="filter ajaxfilter "><?=Yii::t('app', 'Subscriptions');?></div>
    <div class="select_5">
        <select class="form-control selectpicker">
            <option><?=Yii::t('app', 'Order:');?></option>
        </select>
    </div>
</div>