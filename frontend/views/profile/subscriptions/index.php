<?php
    /* @var $this yii\web\View */

    use yii\web\JqueryAsset;

    $this->title = \Yii::t('app', 'Subscriptions(wall) - page title');
?>

<div class="col-xs-9 content">
    <div role="tabpanel" class="custom_tab">

        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#tab1" aria-controls="home" role="tab" data-toggle="tab"><?=Yii::t('app', 'Subscriptions');?></a></li>
        </ul>

        <!-- Tab panes -->
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane tab_body tab_body_pad active" id="tab1">
                <div role="tabpanel">
                    <div class="sort_post sort_by sort_by_comment">
                        <!-- Nav tabs -->
                        <ul class="" role="tablist">
                            <li role="presentation" class="filter active"><a href="#tab1_1" aria-controls="home" role="tab" data-toggle="tab" aria-expanded="true"><?=Yii::t('app', 'Users');?></a></li>
                            <li role="presentation" class="filter"><a href="#tab1_2" aria-controls="profile" role="tab" data-toggle="tab" aria-expanded="false"><?=Yii::t('app', 'Tags');?></a></li>
                        </ul>
                    </div>
                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="tab1_1">
                            <div class="blocks_comment block_subscribers">
                                      
                                <?php foreach ($users as $key => $user): ?>
                                                                        
                                <div class="subscriber">
                                    <div class="header_subscriber clearfix">
                                        <div class="ava">
                                            <a href="<?=$user->profileUrl?>">
                                                <img src="<?=$user->avaUrl?>">
                                            </a>
                                            <div class="quantity_user"><?=$user->profile->rating?></div>
                                        </div>
                                        <p class="subscriber_name"><?=$user->username?></p>
                                        <!-- //@TODO: Add social link here -->
                                        <p class="subscriber_social">http://vk.com/helloworldtestinglogin</p>
                                        <button class="btn btn_unsubscribe user_unsubscribe" data-id="<?=$user->id?>"><?=Yii::t('app', 'Unsubscribe');?></button>
                                    </div>
                                    <div class="info_subscriber">
                                        <div class="item_info_subscriber">
                                            <div>
                                                <img src="/img/icon_time.png">
                                                <!-- //@TODO: add register date here -->
                                                <p class="text_info_subscriber"></p>
                                            </div>
                                        </div>
                                        <div class="item_info_subscriber">
                                            <div>
                                                <img src="/img/icon_comment.png">
                                                <p class="text_info_subscriber"><?=Yii::t('app', '{count} comments', ['count'=>$user->profile->comments_count]);?></p>
                                            </div>
                                        </div>
                                        <div class="item_info_subscriber">
                                            <div>
                                                <img src="/img/icon_news.png">
                                                <p class="text_info_subscriber"><?=Yii::t('app', '{count} posts', ['count'=>$user->profile->posts_count]);?></p>
                                            </div>
                                        </div>
                                        <div class="item_info_subscriber">
                                            <div>
                                                <img src="/img/icon_rating.png">
                                                <!-- //@TODO: add to user pluses count and minuses count -->
                                                <p class="text_info_subscriber">1589+  2589-</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="your_note">
                                        <textarea class="form-control" placeholder="<?=Yii::t('app', 'Zamitka');?>" style="overflow: hidden; word-wrap: break-word; height: 80px;"></textarea>
                                    </div>
                                </div>

                                <?php endforeach ?>

                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="tab1_2">
                            <?php $ch = ''; ?>
                            <?php foreach ($tags as $key => $tag): ?>
                                
                                <?php if (strtolower($tag->text[0]) != strtolower($ch)): ?>
                                    <?php $ch = $tag->text[0] ?>
                                    <div class="block_tags block_ignore_list">
                                    <p class="letter"><?=strtoupper($ch).strtolower($ch)?></p>
                                <?php endif ?>

                                <p class="tags"><?=$tag->text?><img src="/img/close_tags.png" class="close_tags tag_unsubscribe" data-id="<?=$tag->id?>"></p>

                                <?php if ( !isset($tags[$key+1]) || (strtolower($tag->text[0]) != strtolower($tags[$key+1]->text[0]))) : ?>
                                    </div>
                                <?php endif ?>

                            <?php endforeach ?>
                        </div>
                    </div>

                </div>                                   
            </div>

        </div>
    </div>
</div>



<?php 
    $this->registerJsFile('/js/assets/tag/actions.js', ['depends' => [JqueryAsset::className()]]);
    $this->registerJsFile('/js/assets/user/actions.js', ['depends' => [JqueryAsset::className()]]);
 ?>