<?php
    /* @var $this yii\web\View */

    use yii\web\JqueryAsset;

    $this->title = \Yii::t('app', 'Saved (wall) - page title');
?>

<div class="col-xs-9 content">
	<div role="tabpanel" class="custom_tab">

	    <!-- Nav tabs -->
	    <ul class="nav nav-tabs" role="tablist">
	        <li role="presentation" class="active"><a href="#tab1" aria-controls="home" role="tab" data-toggle="tab"><?=Yii::t('app', 'Saved(wall)');?></a></li>
	    </ul>

	    <!-- Tab panes -->
	    <div class="tab-content">
	        <div role="tabpanel" class="tab-pane active" id="tab1">

	            <?php foreach ($posts as $key => $post): ?>
	            	<?=$this->render('../../common/post', ['post'=>$post, 'panel'=>$key?'':$panel])?>
	            <?php endforeach ?>

	        </div>

	    </div>
	</div>
</div>

<?php
	$this->registerJsFile('/js/assets/post/actions.js', ['depends' => [JqueryAsset::className()]]);
	$this->registerJsFile('/js/assets/post/load.js', ['depends' => [JqueryAsset::className()]]);
?>