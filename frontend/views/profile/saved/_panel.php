<div class="sort_post sort_by sort_by_comment">
	<?php 
        $class = '';
        if(in_array($this->context->action->id, ['index', 'posts'])) $class='active'; 
    ?>
    <div class="filter <?=$class?> ajaxfilter" data-target="#tab1" href="#tab1" data-href="/profile/saved/posts" data-toggle="tabajax"><?=Yii::t('app', 'Posts');?></div>

	<?php 
        $class = '';
        if($this->context->action->id == 'comments') $class='active'; 
    ?>
    <div class="filter <?=$class?> ajaxfilter" data-target="#tab1" href="#tab1" data-href="/profile/saved/comments" data-toggle="tabajax"><?=Yii::t('app', 'Comments');?></div>

    <div class="select_5">
        <select class="form-control selectpicker">
            <option><?=Yii::t('app', 'Order:');?></option>
        </select>
    </div>
</div>