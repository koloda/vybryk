<div class="sort_post sort_by sort_by_comment">
    <?php 
        $class = '';
        if(in_array($this->context->action->id, ['index', 'my'])) $class='active'; 
    ?>
    <div class="filter ajaxfilter <?=$class?>" data-target="#tab1" href="#tab1" 
    	data-href="/profile/comments/my" aria-controls="home" role="tab" data-toggle="tabajax">Мої</div>

    <?php 
        $class = '';
        if($this->context->action->id == 'comment-ansvers') $class='active'; 
    ?>
    <div class="filter ajaxfilter <?=$class?>" data-target="#tab1" href="#tab1" data-href="/profile/comments/comment-ansvers" aria-controls="home" role="tab" data-toggle="tabajax">Відповіді на коментарі</div>

	<?php 
        $class = '';
        if($this->context->action->id == 'post-ansvers') $class='active'; 
    ?>
    <div class="filter ajaxfilter <?=$class?>" data-target="#tab1" href="#tab1" data-href="/profile/comments/post-ansvers" aria-controls="home" role="tab" data-toggle="tabajax">Відповіді на пости</div>
    
	<?php 
        $class = '';
        if($this->context->action->id == 'references') $class='active'; 
    ?>
    <div class="filter ajaxfilter <?=$class?>" data-target="#tab1" href="#tab1" data-href="/profile/comments/references" aria-controls="home" role="tab" data-toggle="tabajax">Згадування</div>
</div>