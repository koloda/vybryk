<?php
/* @var $this yii\web\View */

use yii\web\JqueryAsset;

$this->title = \Yii::t('app', 'Profile comments - page title');
?>

<div class="col-xs-9 content">
    <div role="tabpanel" class="custom_tab">

        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a data-href="/profile/comments/my" data-target="#tab1" href="#tab1" aria-controls="home" role="tab" data-toggle="tabajax"><?=Yii::t('app', 'Comments');?></a></li>
            <li role="presentation"><a data-href="/profile/comments/top50" data-target="#tab2" href="#tab2" aria-controls="profile" role="tab" data-toggle="tabajax"><?=Yii::t('app', 'Top50 comments');?></a></li>
        </ul>

        <!-- Tab panes -->
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane tab_body tab_body_pad active" id="tab1">
                
                <?= $this->render('_panel') ?>

                <div class="blocks_comment block_comment_post">
                    
                    <?php foreach ($comments as $key => $comment): ?>
                        <?=$this->render('_comment.php', ['comment' => $comment])?>
                    <?php endforeach ?>

                </div>
            </div>
            <div role="tabpanel" class="tab-pane tab_body tab_body_pad" id="tab2">
                
                

            </div>
        </div>
    </div>
</div>

<?php 
    $this->registerJsFile('/js/assets/post/actions.js', ['depends' => [JqueryAsset::className()]]);
    $this->registerJsFile('/js/assets/post/load.js', ['depends' => [JqueryAsset::className()]]);
?>