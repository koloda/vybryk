<?php 
use \yii\timeago\TimeAgo;
use yii\helpers\Url;

?>

<div class="block_comment clearfix">

    <a href="<?=$comment->user->profileUrl?>" class="ava_user_post">

        <!-- @TODO: add user ava and url (create method User::getProfileUrl()) -->
        <div class="ava_user_post_man">
            <img src="/img/ava_post.png">
        </div>
    </a>

    <div class="info_comment" id="comment-<?=$comment->id?>">
        <p class="name_user">
            <a href="<?=$comment->user->profileUrl?>"><?=$comment->authorName?></a>
        </p>

        <div class="info_post_more clearfix">
            <span class="time_add_post point_after">
                <img src="/img/time_icon.png"><?= TimeAgo::widget(['timestamp' => $comment->created]); ?></span>                                           

            <?php
                $dis = true;
                if (!\Yii::$app->user->isGuest)
                {
                    $dis = false;
                    if(in_array($comment->id, array_keys(\Yii::$app->user->identity->getUserFavoriteComments() )))
                        $dis = true;
                }
            ?>

            <div class="add_post_icon point_after <?php if($dis) echo 'disabled';?>" data-url="<?=Url::to(['/comment/add-to-favorites', 'comment_id' => $comment->id])?>">
                <img src="/img/add_post.png" class="tooltip_text <?php if($dis) echo 'animate_add_post';?>" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Зберегти">
            </div>

        </div>

        <p class="text_comment"><?=$comment->text?></p>

        <div class="down_post rating_block">
            <div class="post_up">

                <!-- COMMENT PLUS -->
                <?php 
                    $dis = true;
                    if (!\Yii::$app->user->isGuest)
                    {
                        $dis = false;
                        if(in_array($comment->id, array_keys(\Yii::$app->user->identity->commentPluses)))
                            $dis = true;
                    }
                ?>

                <div class="post_up_icon <?php if($dis) echo ' disabled '; ?>" data-url="<?=Url::to(['/comment/plus-minus', 'comment_id' => $comment->id, 'sign' => 'plus'])?>"></div>

                <span class="plus-counter">+<?=$comment->pluses_count?></span>
            </div>
            <div class="post_down">
                
                <!-- COMMENT MINUS -->
                <?php 
                    $dis = true;
                    if (!\Yii::$app->user->isGuest)
                    {
                        $dis = false;
                        if(in_array($comment->id, array_keys(\Yii::$app->user->identity->commentMinuses)))
                            $dis = true;
                    }
                ?>

                <div class="post_down_icon <?php if($dis) echo ' disabled '; ?>" data-url="<?=Url::to(['/comment/plus-minus', 'comment_id' => $comment->id, 'sign' => 'minus'])?>"></div>
                
                <span class="minus-counter">-<?=$comment->minuses_count?></span>
            </div>

            <?php if (!\Yii::$app->user->isGuest): ?>
                <span class="reply_comment write-comment-reply" onclick="window.location='/post/view?id=<?=$comment->post_id?>#comment-<?=$comment->id?>'">Відповісти</span>
            <?php endif ?>

        </div>
    </div>
</div>