<?php 
	if(isset($panel))
		echo $panel;
?>

<div class="blocks_comment block_comment_post">
    
    <?php foreach ($comments as $key => $comment): ?>
        <?=$this->render('_comment.php', ['comment' => $comment])?>
    <?php endforeach ?>

</div>