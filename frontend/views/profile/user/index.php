<?php
    /* @var $this yii\web\View */

    use yii\web\JqueryAsset;

    $this->title = \Yii::t('app', 'User page - {username} - page title', ['username'=>$user->username]);
?>

<div class="col-xs-9 content">
    <div class="tab_body tab_body_pad wpapper_user">
        <div class="blocks_user">
            <div class="subscriber user">
                <div class="header_subscriber clearfix">
                    <div class="ava">
                        <img src="<?=$user->avaUrl?>">
                        <div class="quantity_user"><?=$user->profile->rating?></div>
                    </div>
                    <p class="subscriber_name"><?=$user->username?></p>
                    <p class="subscriber_social">http://vk.com/helloworldtestinglogin</p>
                

                    <?php if (!\Yii::$app->user->isGuest): ?>    
                        
                        <?php 
                            $class = 'user_subscribe';
                            $label = \Yii::t('app', 'Subscribe');
                            if(in_array($user->id, array_keys(\Yii::$app->user->identity->getUserUserSubscriptions())))
                            {
                                $class = 'user_unsubscribe';
                                $label = \Yii::t('app', 'Unsubscribe');
                            }
                        ?>

                        <button class="btn button_subscriber <?=$class?>" data-id="<?=$user->id?>" data-label="<?=\Yii::t('app', 'Subscribe')?>" data-ulabel="<?=\Yii::t('app', 'Unsubscribe')?>"><?=$label?></button>                        
    
                        <?php 
                            $class = 'user_ignore';
                            $label = \Yii::t('app', 'Ignore');
                            if(in_array($user->id, array_keys(\Yii::$app->user->identity->getUserUserIgnores())))
                            {
                                $class = 'user_unignore';
                                $label = \Yii::t('app', 'Unignore');
                            }
                        ?>
                        <!-- //@TODO: add some icon -->
                        <button class="btn button_ignor <?=$class?>" data-id="<?=$user->id?>" data-label="<?=\Yii::t('app', 'Ignore')?>" data-ulabel="<?=\Yii::t('app', 'Unignore')?>"><?=$label?></button>

                    <?php endif; ?>

                </div>
                <div class="info_subscriber">
                    <div class="item_info_subscriber">
                        <div>
                            <img src="/img/icon_time.png">
                            <!-- //@TODO: add register date here -->                    
                            <p class="text_info_subscriber">2 роки </p>
                        </div>
                    </div>
                    <div class="item_info_subscriber">
                        <div>
                            <img src="/img/icon_comment.png">
                            <p class="text_info_subscriber"><?=Yii::t('app', '{count} comments', ['count'=>$user->profile->comments_count]);?></p>
                        </div>
                    </div>
                    <div class="item_info_subscriber">
                        <div>
                            <img src="/img/icon_news.png">
                            <p class="text_info_subscriber"><?=Yii::t('app', '{count} posts', ['count'=>$user->profile->posts_count]);?></p>
                        </div>
                    </div>
                    <div class="item_info_subscriber">
                        <div>
                            <img src="/img/icon_rating.png">
                            <!-- //@TODO: add pluses and minuses count to user profile -->
                            <p class="text_info_subscriber">1589+  2589-</p>
                        </div>
                    </div>
                </div>
            </div>                                                                                                                    
        </div>

        <?php
            $posts = $user->getPosts()->orderBy('created DESC')->limit(10)->all();
            $post = $posts[0];
            array_shift($posts);
        ?>
            <div class="post_user">
                <?=$this->render('../../common/post.php', ['post' => $post])?>
            </div>

    </div>
        <?php foreach ($posts as $key => $post): ?>
            <?=$this->render('../../common/post.php', ['post' => $post])?>
        <?php endforeach ?>
</div>


<?php 
    $this->registerJsFile('/js/assets/post/actions.js', ['depends' => [JqueryAsset::className()]]);
    $this->registerJsFile('/js/assets/post/load.js', ['depends' => [JqueryAsset::className()]]);
    $this->registerJsFile('/js/assets/user/actions.js', ['depends' => [JqueryAsset::className()]]);
?>