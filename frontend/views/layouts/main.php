<?php 
use yii\helpers\Html;
use frontend\assets\AppAsset;
use yii\helpers\Url;
use app\components\CommentsWidget;
use app\components\TagsWidget;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);
 ?>
 <?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <?php foreach (Yii::$app->controller->getMetaTags() as $tag) : ?>
            <meta name="<?= $tag->name; ?>" content="<?= $tag->meta; ?>">
        <?php endforeach; ?>

        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>

        <link rel="shortcut icon" type="image/x-icon" href="favicon.ico">   
        <link href='http://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900,900italic&subset=latin,cyrillic-ext' rel='stylesheet' type='text/css'>

        <link href="/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="/css/bootstrap-select.min.css" rel="stylesheet" type="text/css"/>
        <link href="/css/style.css" rel="stylesheet" type="text/css"/>
        <link href="/css/custom-style.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
    <?php $this->beginBody() ?>
        <!-- header-->
        <header id="top">
            <div class="menu_1">
                <div class="container">
                    <div class="row">
                        <div class="logo">
                            <a href="/"><img src="/img/logo.png"></a>
                        </div>
                        <div class="col-xs-8 col-xs-offset-4">
                            <ul>
                                <li><a href="txt.html">Список тегів</a></li>
                                <li><a href="txt.html">Обговорюванні</a></li>
                                <li><a href="txt.html">FAQ</a></li>
                                <li><a href="txt.html">Правила</a></li>
                                <li><a href="txt.html">Реклама</a></li>
                                <li><a href="txt.html">Бан</a></li>
                                <li><a href="txt.html">Новачкам</a></li>
                                <?php foreach (Yii::$app->controller->getUpperMenuItems() as $item) : ?>
                                    <?= Html::tag('li', Html::a($item['title'], $item['url'])) ?>
                                <?php endforeach; ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="menu_2">
                <div class="container">
                    <div class="row">
                        <div class="menu_2_items col-xs-9">
                            <ul>
                                <li <?php if($this->context->id == 'hot'):?>class="active"<?php endif?>>
                                    <a href="/hot"><div class="icon_main_menu icon_main_menu_1"><span class="sptite_icon_menu"></span></div><span>Гаряче</span></a>
                                </li>

                                <li <?php if($this->context->id == 'fresh'):?>class="active"<?php endif?>>
                                    <a href="/fresh"><div class="icon_main_menu icon_main_menu_2"><span class="sptite_icon_menu"></span></div><span>Свіже</span></a>
                                </li>

                                <li <?php if($this->context->id == 'dispute'):?>class="active"<?php endif?>>
                                    <a href="/dispute/actual"><div class="icon_main_menu icon_main_menu_3"><span class="sptite_icon_menu"></span></div><span>Спірне</span></a>
                                </li>

                                <li>
                                    <a href="confesing.html"><div class="icon_main_menu icon_main_menu_4"><span class="sptite_icon_menu"></span></div><span>Зізнання</span></a>
                                </li>

                                <li>
                                    <a href="gold.html"><div class="icon_main_menu icon_main_menu_5"><span class="sptite_icon_menu"></span></div><span>Золото</span></a>
                                </li>

                                <li <?php if($this->context->id == 'best'):?>class="active"<?php endif?>>
                                    <a href="/best"><div class="icon_main_menu icon_main_menu_6"><span class="sptite_icon_menu"></span></div><span>Найкраще</span></a>
                                </li>
                            </ul>
                        </div>
                        <div class="wpapper_form">
                            <form class="block_form">
                                <input class="form-control" type="text" name="searh">
                                <button class="btn"><img src="/img/search_loop.png"></button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <!--end header-->

        <!-- main-->
        <section class="main">
            <div class="container">
                <div class="row">

                    <?=$content?>

                    <div class="col-xs-3 block_sidebar">
                        <div class="sidebar">

                        <?php if (Yii::$app->user->isGuest): ?>

                            <div class="left_border_sidebar"></div>
                            <div class="sidebar_login_reg">
                                <ul class="nav nav-tabs" role="tablist" id="myTab">
                                    <li role="presentation" class="active nav-tabs1"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Авторизація</a></li>
                                    <li role="presentation" class="nav-tabs2"><a id="tab-button-register" href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Реєстрація</a></li>
                                </ul>

                                <div class="tab-content">

                                    <div role="tabpanel" class="tab-pane active" id="home">
                                        <form id="loginForm" method="post">
                                            <div class="form-group">
                                                <input class="form-control" required="" name="login-form[login]" type="text" placeholder="Логін">
                                            </div>
                                            <div class="form-group form-group_" id="mypass-wrap">

                                                <input class="form-control" id="our_pass" required="" name="login-form[password]" type="password" placeholder="Пароль">                                       
                                                <div class="tooltip_pass tooltip_text" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Забули пароль"><img src="/img/tooltip_pass.png" data-toggle="modal" data-target="#myModal"></div>                                               
                                                <div onclick="pass();" class="change_pass tooltip_text" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Показати пароль">
                                                    <img src="/img/eye.png">
                                                </div>
                                            </div>
                                            <div class="alert alert-danger" style="display:none;" id="loginFormAlert"></div>
                                            <div class="checkbox">
                                                <label>Запам’ятати мене<input type="checkbox" class="ios-switch green  bigswitch" name="login-form[rememberMe]" value="1"><div><div></div></div></label> 
                                            </div>
                                            <!-- <a href="home.html" class="btn center-block">Авторизуватись</a> -->
                                            <button class="btn center-block" type="submit">Авторизуватись</button>
                                        </form>
                                    </div>
                                    

                                    <?php if (\Yii::$app->session->has('connect_account')): ?>

                                    <div role="tabpanel" class="tab-pane" id="profile">

                                        <div class="alert alert-info">
                                            <?= Yii::t('user', 'Looks like this is first time you are using {0} account to sign in to {1}', [\Yii::$app->session->get('connect_account'), Yii::$app->name]) ?>.
                                            <?= Yii::t('user', 'Connect this account by entering desired username and your email address below') ?>.
                                            <?= Yii::t('user', 'You will never have to use this form again') ?>.
                                        </div>

                                        <form method="post" id="connectForm">
                                            <div class="form-group">
                                                <input class="form-control" required="" name="User[username]" type="text" placeholder="Логін">
                                            </div>
                                            <div class="form-group">
                                                <input class="form-control" required="" name="User[email]" type="email" placeholder="E-mail">
                                            </div>
                                            <div class="alert alert-danger" style="display:none;" id="connectFormAlert"></div>
                                            <button class="btn center-block" type="submit">Завершити</button>
                                        </form>
                                    </div>

                                    <?php else: ?>
                                        
                                    <div role="tabpanel" class="tab-pane" id="profile">
                                        <form method="post" id="registerForm">
                                            <div class="form-group">
                                                <input class="form-control" required="" name="register-form[username]" type="text" placeholder="Логін">
                                            </div>
                                            <div class="form-group">
                                                <input class="form-control" required="" name="register-form[email]" type="email" placeholder="E-mail">
                                            </div>
                                            <div class="form-group form-group_" id="mypass-wrap">
                                                <input class="form-control" id="our_pass1" required="" name="register-form[password]" type="password" placeholder="Пароль">
                                                <div onclick = "pass1();" class="change_pass tooltip_text" data-toggle="tooltip" data-placement="bottom" title="Показати пароль">
                                                    <img src="/img/eye.png">
                                                </div>
                                            </div>
                                            <div class="alert alert-danger" style="display:none;" id="registerFormAlert"></div>
                                            <div class="alert alert-success" style="display:none;" id="registerFormSuccessAlert"></div>
                                            <button class="btn center-block" type="submit">Зареєструвати</button>
                                            <p class="or_text">або</p>
                                            <a href="/user/security/auth?authclient=twitter"><img src="/img/social_tw.png" class="social_icon_log social_icon_log_1"></a>
                                            <a href="/user/security/auth?authclient=facebook"><img src="/img/social_fb.png" class="social_icon_log social_icon_log_2"></a>
                                            <a href="/user/security/auth?authclient=vkontakte"><img src="/img/social_vk.png" class="social_icon_log"></a>
                                        </form>
                                    </div>

                                    <?php endif ?>

                                </div>
                            </div>

                            <?php else: ?>

                            <div class="header-sidebar">
                                <div class="ava">
                                    <img src="/img/ava.png">
                                    <div class="quantity_user">10251</div>
                                </div>
                                <div class="block_name_user">
                                    <p class="name_user"><?=\Yii::$app->user->identity->username?></p>
                                    <a href="<?=Url::to('/user/security/logout')?>" class="log_out"><img src="/img/log_out.png" class="tooltip_text" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Вийти"></a>
                                    <div class="block_add_post">
                                        <a href="<?=Url::to('/compose/text')?>" class="add_post">Додати пост<span></span></a>
                                    </div>
                                </div>
                            </div>

                            <div class="line_gr_1"></div>

                            <div class="sidebar_menu">
                                <div class="item_menu_sidebar">
                                    <a href="/profile/wall">
                                        <img src="/img/icon_1.png"><?=Yii::t('app', 'Wall');?>
                                    </a>
                                </div>
                                <div class="item_menu_sidebar">
                                    <a href="/profile/comments">
                                        <img src="/img/icon_2.png"><?=Yii::t('app', 'Comments');?>
                                    </a>
                                </div>
                                <div class="item_menu_sidebar">
                                    <a href="/profile/subscriptions">
                                        <img src="/img/icon_3.png"><?=Yii::t('app', 'Subscriptions');?>
                                    </a>
                                </div>
                                <div class="item_menu_sidebar">
                                    <a href="saved.html">
                                        <img src="/img/icon_4.png"><?=Yii::t('app', 'Saved (user profile)');?>
                                    </a>
                                </div>
                                <div class="item_menu_sidebar">
                                    <a href="settings.html">
                                        <img src="/img/icon_5.png"><?=Yii::t('app', 'Settings', array());?>
                                    </a>
                                </div>
                            </div>

                            <?php endif ?>

                            <div class="line_gr_1"></div>
                            <div class="sidebar_user">
                                <div class="top_comment">

                                    <?= CommentsWidget::widget() ?>

                                    <div class="line_1"></div>                                    
                                    <div class="advice">
                                        <img src="/img/close.png" class="close_advice">
                                        <a href="txt.html">
                                            <div class="text_advice">
                                                <p class="text_advice_1">Не забувай лайкнути ближнього свого</p>
                                                <p class="text_advice_2">Поставивши лайк Ви піднімаєте рейтинг користувача, тим самим відкриваєте їй/йому нові можливості</p>
                                            </div>
                                            <div class="footer_advice">
                                                <img src="/img/logo_2.png">
                                                <div class="message">
                                                    <p class="text_footer_advice_1">Vybryk.com</p>
                                                    <p class="text_footer_advice_2">Тільки корисне!</p>
                                                </div>
                                            </div>
                                        </a>
                                    </div>                                    
                                    <div class="line_1"></div>
                                    <?= TagsWidget::widget() ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--end main-->

        <!-- footer-->
        <footer>
            <div class="container">
                <div class="row">
                    <div class="col-xs-8">
                        <p class="copy">
                            <span class="copy_icon">&copy;</span>
                            <span class="copy_text_1">Unknown.</span>
                            <span class="copy_text_2">inc</span>
                        </p>
                        <ul class="menu_footer">
                            <li><a href="txt.html">Список тегів</a></li>
                            <li><a href="txt.html">Обговорюванні</a></li>
                            <li><a href="txt.html">FAQ</a></li>
                            <li><a href="txt.html">Правила</a></li>
                            <li><a href="txt.html">Реклама</a></li>
                            <li><a href="txt.html">Бан</a></li>
                            <li><a href="txt.html">Новачкам</a></li>
                        </ul>
                    </div>
                    <div class="col-xs-4">
                        <span class="email">mvm.lenko@gmail.com</span>
                        <ul class="social_footer">
                            <li><a href=""><img src="/img/twitter_icon.png"></a></li>
                            <li><a href=""><img src="/img/facebook_icon.png"></a></li>
                            <li><a href=""><img src="/img/vk_icon.png"></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </footer>
        <!--end -->

        <a href="#top" class="up_arrow animate" style="display: inline;"><img src="/img/up_arrow.png" alt=""></a>

        <!-- Modal -->
        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Modal title</h4>
                    </div>
                    <div class="modal-body">
                        ...
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary">Save changes</button>
                    </div>
                </div>
            </div>
        </div>

        <?php $this->endBody() ?>
        
        <script type="text/javascript" src="/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="/js/jquery.customSelect.js"></script>
        <script type="text/javascript" src="/js/jquery.scrollTo-1.4.3.1-min.js"></script>
        <script type="text/javascript" src="/js/bootstrap-select.min.js"></script>
        <script type="text/javascript" src="/js/jquery.form.min.js"></script>
        <script type="text/javascript" src="/js/script.js"></script>

        <script>
    
        $(function(){
            $('#loginForm').ajaxForm({
                url : '/user/security/login',
                dataType: 'json',
                success : function(data) {
                    var errors = [];
                    var errorsStr = '';

                    try 
                    {
                        var parsedData = data;

                        if(typeof(parsedData['login-form-login']) != 'undefined')
                            errors = errors.concat( parsedData['login-form-login'] );

                        if(typeof(parsedData['login-form-password']) != 'undefined')
                            errors = errors.concat( parsedData['login-form-password']);

                        console.log(errors)



                        if(errors.length)
                            $('#loginFormAlert').html(errors.join('. ')).slideDown();
                        else
                        {
                            $('#loginFormAlert').html('').slideUp();
                            window.location.reload();
                        }
                    }
                    catch (e) {
                        
                    }
                }
            });

            $('#registerForm').ajaxForm({
                url : '/user/registration/register',
                dataType: 'json',
                success : function(data) {
                    var errors = [];
                    var errorsStr = '';

                    try 
                    {
                        var parsedData = data;

                        if(typeof(parsedData['register-form-username']) != 'undefined')
                            errors = errors.concat( parsedData['register-form-username'] );

                        if(typeof(parsedData['register-form-email']) != 'undefined')
                            errors = errors.concat( parsedData['register-form-email'] );

                        if(typeof(parsedData['register-form-password']) != 'undefined')
                            errors = errors.concat( parsedData['register-form-password']);

                        if(errors.length)
                            $('#registerFormAlert').html(errors.join('. ')).slideDown();
                        else
                            $('#registerFormAlert').html('').slideUp();

                        if(typeof(parsedData['success-message']) != 'undefined')
                            $('#registerFormSuccessAlert').html(parsedData['success-message']).slideDown();
                        else
                            $('#registerFormSuccessAlert').html('').slideUp();
                    }
                    catch (e) {}
                }
            });
        });
        </script>

        <?php if (\Yii::$app->session->has('connect_account')): ?>
        <script>
            $(function(){
                var userId = <?=\Yii::$app->session->get('account_id')?>;

                $('#tab-button-register').click()

                $('#connectForm').ajaxForm({
                    url : '/user/registration/connect?account_id='+userId,
                    success : function(data) {
                        var errors = [];
                        var errorsStr = '';

                        try 
                        {
                            var parsedData = JSON.parse(data);

                            if(typeof(parsedData['username']) != 'undefined')
                                errors = errors.concat( parsedData['username'] );

                            if(typeof(parsedData['email']) != 'undefined')
                                errors = errors.concat( parsedData['email']);

                            if(errors.length)
                                $('#connectFormAlert').html(errors.join('. ')).slideDown();
                            else
                            {
                                $('#connectFormAlert').html('').slideUp();
                                window.location.reload();
                            }
                        }
                        catch (e) 
                        {
                            window.location.reload();
                        }
                    }
                });
            });
        </script>

        <?php 
        \Yii::$app->session->remove('connect_account');
        \Yii::$app->session->remove('account_id');
         ?>

        <?php endif ?>

    </body>
</html>
<?php $this->endPage() ?>