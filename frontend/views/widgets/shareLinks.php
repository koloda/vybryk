<?php
	use ijackua\sharelinks\ShareLinks;
?>

<ul class="social_icon point_after">
    <li><a href="<?=$this->context->shareUrl(ShareLinks::SOCIAL_TWITTER)?>"><img src="/img/twitter_icon.png"></a></li>
    <li><a href="<?=$this->context->shareUrl(ShareLinks::SOCIAL_FACEBOOK)?>"><img src="/img/facebook_icon.png"></a></li>
    <li><a href="<?=$this->context->shareUrl(ShareLinks::SOCIAL_VKONTAKTE)?>"><img src="/img/vk_icon.png"></a></li>
</ul>