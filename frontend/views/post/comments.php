            <?php if ($post->comments_count): ?>
            
            <?php foreach ($post->commentsTree as $comment): ?>
            
            <div class="block_comment clearfix">

                <!-- RENDER COMMENT HERE -->
                <?= $this->render('../comment/comment', ['comment' => $comment]) ?>

                <!-- CHILDREN HERE -->
                <?php if (count($comment->childs)): ?>
                    <?php foreach ($comment->childs as $key=>$child): ?>

                        <div class="answer_comment <?php if(!$key):?>first_answer_comment<?php endif?>">
                            <?= $this->render('../comment/comment', ['comment' => $child]) ?>
                        </div>

                    <?php endforeach ?>
                <?php endif ?>
                
                <div class="vertical_line"></div>
            </div>

            <?php endforeach ?>

            <?php endif ?>
            
            <?php if (!\Yii::$app->user->isGuest): ?>
                <div class="block_add_comment" style="bottom: 0px; display: none;">          
                </div>
            <?php endif ?>

<?php $this->registerJs(<<<JS

    $('.tooltip_text').tooltip({
        animation: true,
        delay: 100
    });

    $('.add_post_icon img').click(function() {
        $(this).addClass("animate_add_post");
    });

    /*Show/hide comment*/
    $('.show_comment').click(function() {
        //$(this).parent().parent().next('.answer_comment').slideToggle();
        $(this).parent().parent().parent().find('.answer_comment').slideToggle();
        if ($(this).text() === "Показати") {
            $(this).text("Сховати");
            $(this).css('background-image', 'url(/img/arrow_hide_comment.png)');
        }
        else
        {
            $(this).text("Показати");
            $(this).css('background-image', 'url(/img/arrow_show_comment.png)');
        }
    });

    $('.reply_comment').click(function() {
        var author = $(this).data('author');
        var parent_id = $(this).data('parentid');
        var original_parent_id = $(this).data('origparentid');

        console.log(parent_id);

        $('.field-comment-parent_id input').val(parseInt(parent_id));
        $('.field-comment-original_parent_id input').val(parseInt(original_parent_id));
        $('.block_add_comment textarea.form-control').focus();
        $('.block_add_comment textarea.form-control').val('@' + author + ', ');
        $('.block_add_comment').fadeIn();
    });

JS
                             ); ?>