<?php
/* @var $this yii\web\View */
use \yii\timeago\TimeAgo;
use yii\helpers\Url;
use yii\web\JqueryAsset;
use \ijackua\sharelinks\ShareLinks;

$this->title = $post->title;
$this->registerMetaTag(['name'=>'title', 'content'=>$post->title], 'title');
$this->registerMetaTag(['name'=>'description', 'content'=>$post->description?:$post->text], 'description');
?>


<div class="col-xs-9 content">
    <div class="post">
        <div class="body_post">
            <div class="header_post">
                <p class="title_post"><?=$post->title?></p>

                <?php if (strlen($post->description)): ?>
                    <p class="subtitle_post"><?=$post->description?></p>
                <?php endif ?>

                <!-- //@TODO: show this icon if it is scrollpost -->
<!--                 <div class="indicator_post">
                    <img src="img/icon_post_1.png" class="tooltip_text" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Довгопост">
                </div> -->

                <div class="info_post">
                    <a href="<?=$post->user->profileUrl?>">
                        <div class="ava_user_post ava_user_post_man">
                            <img src="<?=$post->user->avaUrl?>">
                        </div>
                    </a>

                    <p class="name_user"><a href=""><?=$post->user->username?></a></p>

                    <div class="info_post_more">
                        <span class="time_add_post point_after"><img src="/img/time_icon.png"><?= TimeAgo::widget(['timestamp' => $post->created]); ?></span>

                        <a href="#comment"><span class="quantity_comment point_after"><img src="/img/comment_icon.png"><?=$post->comments_count?> Коментарів</span></a>
                        
                        <?= ShareLinks::widget(['viewName' => '@app/views/widgets/shareLinks.php', 'url' => Url::to(['/post/view', 'id'=>$post->id], true), 'linkSelector' => '.social_icon a' ] ) ?>

                        <?php if (!\Yii::$app->user->isGuest): ?>
                        <?php 
                            $dis = false;
                            if(in_array($post->id, array_keys(\Yii::$app->user->identity->getUserFavorites() )))
                                $dis = true;
                        ?>
                        
                        <div class="add_post_icon point_after <?php if($dis) echo 'disabled';?>" data-url="<?=Url::to(['/post/add-to-favorites', 'post_id' => $post->id])?>">
                            <img src="/img/add_post.png" class="tooltip_text <?php if($dis) echo 'animate_add_post';?>" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Зберегти">
                        </div>
                        <?php endif; ?>

                        <!-- //@TODO: realize gold -->
                        <div class="gold_post_icon">
                            <img src="/img/gold_post.png" class="tooltip_text" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Позолочено">
                        </div>                                                                                     

                    </div>
                </div>
            </div>


            <!-- //@TODO: if post have picture, they (picture) must have class "image_src" -->
            <p class="text_body_post">
                <?=$post->text?>
            </p>

            <div class="down_post rating_block">

                <div class="post_up">

                    <!-- PLUS -->
                    <?php $dis = true;
                        if (!\Yii::$app->user->isGuest)
                        {
                            $dis = false;
                            if(in_array($post->id, array_keys(\Yii::$app->user->identity->pluses)))
                                $dis = true;
                        }
                    ?>
                    
                    <div class="post_up_icon <?php if($dis) echo ' disabled '; ?>" data-url="<?=Url::to(['/post/plus-minus', 'post_id' => $post->id, 'sign' => 'plus'])?>"></div>

                    <span class="plus-counter">+<?=$post->pluses_count?></span>
                </div>

                <div class="post_down">

                    <!-- MINUS -->
                    <?php $dis = true;
                        if (!\Yii::$app->user->isGuest)
                        {
                            $dis = false;
                            if(in_array($post->id, array_keys(\Yii::$app->user->identity->minuses)))
                                $dis = true;
                        }
                    ?>

                    <div class="post_down_icon <?php if($dis) echo ' disabled '; ?>" data-url="<?=Url::to(['/post/plus-minus', 'post_id' => $post->id, 'sign' => 'minus'])?>"></div>

                    <span class="minus-counter">-<?=$post->minuses_count?></span>
                </div>

                <!-- //@TODO: implement this functionality -->
                <div class="negative_post tooltip_text" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Поскаржитися">
                    <div class="negative_post_icon"></div>
                </div>
            </div>

            <div class="block_tags_post_page">
            <?php foreach ($post->postTags as $tag): ?>
                <a href="<?=Url::to(['/tag/index', 'id'=>$tag->id])?>" class="tags">#<?=$tag->text?></a>
            <?php endforeach ?>
            </div>

        </div>


        <div class="footer_post footer_post_page">
            <p class="quantity_comment_post"><?=$post->comments_count?> Коментарів</p>
            
            <!-- //@TODO: implement this section -->
            <div class="select_6 ">
                <select class="form-control selectpicker " style="display: none;">
                    <option>Рейтинг</option>
                    <option>24год</option>
                    <option>24год</option>
                </select><div class="btn-group bootstrap-select form-control dropup"><button type="button" class="btn dropdown-toggle selectpicker btn-default" data-toggle="dropdown" title="Рейтинг" aria-expanded="false"><span class="filter-option pull-left">Рейтинг</span>&nbsp;<span class="caret"></span></button><div class="dropdown-menu open" style="max-height: 2220px; overflow: hidden; min-height: 0px;"><ul class="dropdown-menu inner selectpicker" role="menu" style="max-height: 2217px; overflow-y: auto; min-height: 0px;"><li data-original-index="0" class="selected"><a tabindex="0" class="" data-normalized-text="<span class=&quot;text&quot;>Рейтинг</span>"><span class="text">Рейтинг</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="1"><a tabindex="0" class="" data-normalized-text="<span class=&quot;text&quot;>24год</span>"><span class="text">24год</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="2"><a tabindex="0" class="" data-normalized-text="<span class=&quot;text&quot;>24год</span>"><span class="text">24год</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li></ul></div></div>
            </div>

        </div>

        <!-- COMMENTS -->

        <div class="block_comment_post block_comment_post_page" id="comment">
            <?=$this->render('comments', ['post' => $post])?>
        </div>

    </div>
</div>

<?php 
    $this->registerJsFile('/js/assets/post/actions.js', ['depends' => [JqueryAsset::className()]]);
 ?>

 <?php 
    
    $this->registerJs(<<<JS

                    function loadComments(post_id)
                    {
                        $('.block_comment_post_page#comment').load('/post/comments?post_id=' + post_id, function(){
                            loadCommentForm();
                        });
                    }

                    function loadCommentForm() {
                        $('.block_add_comment').load('/comment/compose?post_id='+postId, function(){
                            $(window).scrollTo('#comment-' + freshCommentId, 300);
                        });
                    }


                    var postId = $post->id;
                    var commentWasSent = false;
                    var freshCommentId = 0;
                
                    //loadComments(postId);
                    loadCommentForm();

                    // serialize form, render response and close modal
                    $('.block_comment_post_page').on('click', 'form#create-comment-form .btn_send_comment', function(e){
                        var form  = $(this).closest('#create-comment-form');
                        if(!commentWasSent)
                        {    
                            commentWasSent = true;

                            $.post(
                                form.attr("action"), // serialize Yii2 form
                                form.serialize(),
                                'json'
                            )
                            .done(function(result) {
                                commentWasSent = false;
                                freshCommentId = result.id;
                                form.find('textarea').val('');
                                loadComments(postId);
                            })
                            .fail(function() {
                                console.log("server error");
                            });
                        }

                        e.preventDefault();
                        return false;
                    });

JS
                      );
  ?>