<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use frontend\assets\Select2Asset;

$this->title = Yii::t('app', 'Compose new text post - (page title)');

/* @var $this yii\web\View */
/* @var $model app\models\Post */
/* @var $form ActiveForm */
?>

<div class="col-xs-9 content">
    <div class="custom_tab" >
        <ul class="nav nav-tabs" >
            <li class=""><a href="#tab1" aria-controls="home" role="tab" data-toggle="tab" aria-expanded="false">З картинкою</a></li>
            <li class="active"><a href="/compose/text" aria-controls="profile" role="tab" data-toggle="tab" aria-expanded="true">Текстовий</a></li>
            <li class=""><a href="#tab3" aria-controls="messages" role="tab" data-toggle="tab" aria-expanded="false">Cкролпост</a></li>
        </ul>
    </div>

    <div class="tab-content">
        <div role="tabpanel" class="tab-pane tab_body active" id="tab2">

            <?php $form = ActiveForm::begin([
                'id'                    => 'create-text-post-form',
                'enableAjaxValidation'  => true,
                'enableClientValidation' => true,
                // 'validateOnBlur'         => false,
                // 'validateOnType'         => false,
                'validateOnChange'      => true,
                'validateOnSubmit'      => true,
            ]); ?>

            <?=$form->field($model, 'title', ['template' => '{input}{error}', 'inputOptions' => [
                    // 'required'      => '',
                    'placeholder'   => 'Заголовок',
                    'class'         => 'form-control',
                    'tabindex'      => 1
                ]
            ])?>

            <?=$form->field($model, 'description', ['template' => '{input}{error}', 'inputOptions' => [
                    // 'required'      => '',
                    'placeholder'   => 'Опис (не обов\'язково)',
                    'class'         => 'form-control',
                    'tabindex'      => 2
                ]
            ])?>

            <?=$form->field($model, 'text', ['template' => '{input}{error}', 'inputOptions' => [
                    // 'required'      => '',
                    'placeholder'   => 'Текст',
                    'class'         => 'form-control',
                    'style'         => 'overflow: hidden; word-wrap: break-word; height: 216px;',
                    'tabindex'      => 3
                ]
            ])->textarea()?>

            <div class="form-group">
                
            <?=$form->field($model, 'tags', ['template' => '{input}{error}', 'inputOptions' => [
                    // 'required'      => '',
                    'placeholder'   => 'Теги',
                    'class'         => 'form-control',
                    'style'         => 'overflow: hidden; word-wrap: break-word; height: 216px;',
                    'tabindex'      => 4,
                    'multiple'      => 'multiple',
                ]
            ])->dropDownList(
                [],
                [
                    'prompt'    => 'Почніть вводити тег'
                ]
            )?>

            </div>
            
            <?= Html::submitButton('Опублікувати', ['class' => 'btn btn_tab']) ?>

            <?php ActiveForm::end(); ?>

        </div>
    </div>
</div>

<?php 
$this->registerAssetBundle(Select2Asset::className());

$this->registerJs(<<<JS

    $('#post-tags').select2({
  ajax: {
    url: "/compose/tags",
    dataType: 'json',
    delay: 150,
    id : function(item) {
        return item.text;
    },
    data: function (params) {
      return {
        q: params.term, // search term
        //page: params.page
      };
    },
    processResults: function (data, page) {
      // parse the results into the format expected by Select2.
      // since we are using custom formatting functions we do not need to
      // alter the remote JSON data
      return {
        results: data.results
      };
    },
  },
  minimumInputLength: 2,
});

JS
                  );


$this->registerCss(<<<CSS
   .select2-selection {
        min-height: 62px;
        padding-left: 20px;
        -webkit-border-radius: 5px;
        -moz-border-radius: 5px;
        border-radius: 5px;
        border: 3px solid #edf1f2 !important;
        background-color: #fff;
        font-size: 18px;
        font-weight: 700;
        color: #233238;
        font-family: 'Century gothic';
        box-shadow: none;
   }

   .select2-selection__choice {
        padding: 10px !important;
        background-color: #32b287 !important;
        color: #fff !important;
        border: none !important;
   }

   .select2-container--default .select2-selection--multiple .select2-selection__choice__remove {
        margin-right: 7px;
        color: #fff;
        transition: all 0.2s linear;
   }

   .select2-container--default .select2-selection--multiple .select2-selection__choice__remove:hover {
        transform: rotate(180deg);
        color: #fff;
   }
CSS
                   );

 ?>