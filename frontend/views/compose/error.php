<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Post */
/* @var $form ActiveForm */
?>

<div class="col-xs-9 content">
    <div class="custom_tab" >
        <ul class="nav nav-tabs" >
            <li class=""><a href="#tab1" aria-controls="home" role="tab" data-toggle="tab" aria-expanded="false">З картинкою</a></li>
            <li class="active"><a href="/compose/text" aria-controls="profile" role="tab" data-toggle="tab" aria-expanded="true">Текстовий</a></li>
            <li class=""><a href="#tab3" aria-controls="messages" role="tab" data-toggle="tab" aria-expanded="false">Cкролпост</a></li>
        </ul>
    </div>

    <div class="tab-content">
        <div role="tabpanel" class="tab-pane tab_body active" id="tab2">
            <div class="alert alert-danger">
                <?=$error?>
            </div>
        </div>
    </div>
</div>