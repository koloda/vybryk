<?php
/* @var $this yii\web\View */
/* @var $model frontend\models\StaticPage */
?>
<div class="col-xs-9 content">
    <div class="post">
        <div class="body_post">
            <div class="header_post">
                <p class="title_post">
                    <?= $model->title ?>
                </p>
            </div>
            <p class="text_body_post">
                <?= $model->content ?>
            </p>
        </div>
        <div class="footer_post"></div>
    </div>
</div>