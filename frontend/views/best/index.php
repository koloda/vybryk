<?php
/* @var $this yii\web\View */

use yii\web\JqueryAsset;

$this->title = \Yii::t('app', 'Best - page title');
?>

<div class="col-xs-9 content">
    <?php foreach ($posts as $post): ?>
        <?php echo $this->render('../common/post', ['post' => $post]); ?>
    <?php endforeach ?>
</div>

<?php 
    $this->registerJsFile('/js/assets/post/actions.js', ['depends' => [JqueryAsset::className()]]);
    $this->registerJsFile('/js/assets/post/load.js', ['depends' => [JqueryAsset::className()]]);
?>