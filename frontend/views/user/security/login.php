<?php

/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dektrium\user\widgets\Connect;

/**
 * @var yii\web\View                   $this
 * @var dektrium\user\models\LoginForm $model
 * @var dektrium\user\Module           $module
 */
?>
<?php $this->beginContent('@app/views/layouts/empty.php'); ?>
<form>
    <?php $form = ActiveForm::begin([
        'id'                     => 'login-form',
        'enableAjaxValidation'   => true,
        'enableClientValidation' => true,
        'validateOnBlur'         => false,
        'validateOnType'         => false,
        'validateOnChange'       => true,
        'validateOnSubmit'      => false,
        'action'                => ['user/security/login']
    ]) ?>

    <div class="form-group">
        <?= $form->field($model, 'login', ['template' => '{input}', 'inputOptions' => ['required' => '', 'placeholder'=>"Логін", 'class' => 'form-control', 'tabindex' => '1']])->textInput();?>
    </div>
    <div class="form-group form-group_" id="mypass-wrap">
        <?= $form->field($model, 'password', ['template' => '{input}', 'inputOptions' => ['class' => 'form-control', 'placeholder'=>"Пароль", 'id'=>'our_pass', 'tabindex' => '2']])->passwordInput() ?>
        
        <div class="tooltip_pass tooltip_text" data-toggle="tooltip" data-placement="bottom" title="Забули пароль"><img src="img/tooltip_pass.png" data-toggle="modal" data-target="#myModal"></div>                                               
        <div onclick = "pass();" class="change_pass tooltip_text" data-toggle="tooltip" data-placement="bottom" title="Показати пароль">
            <img src="img/eye.png">
        </div>
    </div>
    <div class="checkbox">
        <label>Запам’ятати мене<?=$form->field($model, 'rememberMe', ['template' => '{input}', 'inputOptions' => ['class' => 'ios-switch green  bigswitch']])->checkbox(['tabindex' => '4'])?><div><div></div></div></label> 
    </div>
    <!-- <a href="home.html" class="btn center-block">Авторизуватись</a> -->
    <button class="btn center-block" type="submit">Авторизуватись</button>

    <?php ActiveForm::end(); ?>

<?php $this->endContent();