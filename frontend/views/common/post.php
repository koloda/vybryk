<?php 
    use \yii\timeago\TimeAgo;
    use yii\helpers\Url;
    use \ijackua\sharelinks\ShareLinks;
?>

<div class="post">

    <?php if(isset($panel)) echo $panel; ?>
    
    <div class="body_post">
        <div class="header_post">
            <p class="title_post"><?=$post->title?></p>

            <?php if (strlen($post->description)): ?>
                <p class="subtitle_post"><?=$post->description?></p>
            <?php endif ?>

            <!-- //@TODO: show this icon if it is scrollpost -->
            <!-- <div class="indicator_post">
                <img src="/img/icon_post_1.png" class="tooltip_text" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Скролпост">
            </div> -->

            <div class="info_post">
                <a href="<?=$post->user->profileUrl?>">
                    <div class="ava_user_post ava_user_post_woman">
                        <img src="<?=$post->user->avaUrl?>">
                    </div>
                </a>

                <p class="name_user"><a href=""><?=$post->user->username?></a></p>

                <div class="info_post_more">

                    <span class="time_add_post point_after"><img src="/img/time_icon.png"><?= TimeAgo::widget(['timestamp' => $post->created]); ?></span>

                    <a href="<?=Url::to(['/post/view', 'id'=>$post->id])?>#comment"><span class="quantity_comment point_after"><img src="/img/comment_icon.png"><?=$post->comments_count?> Коментарів</span></a>
                    
                    <?= ShareLinks::widget(['viewName' => '@app/views/widgets/shareLinks.php', 'url' => Url::to(['/post/view', 'id'=>$post->id], true), 'linkSelector' => '.social_icon a' ] ) ?>

                    <?php if (!\Yii::$app->user->isGuest): ?>
                    <?php 
                        $dis = false;
                        if(in_array($post->id, array_keys(\Yii::$app->user->identity->userFavorites)))
                            $dis = true;
                    ?>
                    
                    <div class="add_post_icon point_after <?php if($dis) echo 'disabled';?>" data-url="<?=Url::to(['/post/add-to-favorites', 'post_id' => $post->id])?>">
                        <img src="/img/add_post.png" class="tooltip_text <?php if($dis) echo 'animate_add_post';?>" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Зберегти">
                    </div>
                    <?php endif; ?>

                    <!-- //@TODO: realize gold -->
                    <div class="gold_post_icon">
                        <img src="/img/gold_post.png" class="tooltip_text" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Золото">
                    </div>                                                                                     
                </div>
            </div>

        </div>
        <p class="text_body_post">
            <?=$post->text?>
        </p>

        <div class="down_post">

            <a href="<?=Url::to(['/post/view', 'id'=>$post->id])?>" class="open_post">Відкрити пост</a>

            <div class="post_up">
                <?php if (!\Yii::$app->user->isGuest): ?>
                <?php 
                    $dis = false;
                    if(in_array($post->id, array_keys(\Yii::$app->user->identity->pluses)))
                        $dis = true;
                ?>
                <div class="post_up_icon <?php if($dis) echo ' disabled '; ?>" data-url="<?=Url::to(['/post/plus-minus', 'post_id' => $post->id, 'sign' => 'plus'])?>"></div>
                <?php endif ?>

                <span class="plus-counter">+<?=$post->pluses_count?></span>
            </div>

            <div class="post_down">
                <?php if (!\Yii::$app->user->isGuest): ?>
                <?php 
                    $dis = false;
                    if(in_array($post->id, array_keys(\Yii::$app->user->identity->minuses)))
                        $dis = true;
                ?>
                <div class="post_down_icon <?php if($dis) echo ' disabled '; ?>" data-url="<?=Url::to(['/post/plus-minus', 'post_id' => $post->id, 'sign' => 'minus'])?>"></div>
                <?php endif ?>

                <span class="minus-counter">-<?=$post->minuses_count?></span>
            </div>
        </div>
    </div>
    <div class="footer_post">
        <?php foreach ($post->postTags as $tag): ?>
        <a href="<?=Url::to(['/tag/index', 'id'=>$tag->id])?>" class="tags">#<?=$tag->text?></a>
        <?php endforeach ?>
    </div>
</div>