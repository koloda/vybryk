<?php
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
?>

<div class="ava_add_comment">
    <!-- //@TODO: add user's avatar here -->
    <img src="/img/ava.png">
</div>

<div class="add_comment clearfix">

    <?php $form = ActiveForm::begin([
        'id'                    => 'create-comment-form',
        'enableAjaxValidation'  => true,
        'enableClientValidation'=> true,
        'validateOnType'        => false,
        'validateOnChange'      => false,
        'validateOnBlur'        => false,
        'validateOnSubmit'      => true,
    ]); ?>

    <?=$form->field($model, 'text', ['template' => '{input}{error}', 'inputOptions' => [
            'required'      => '',
            'placeholder'   => 'Введіть текст коментаря',
            'class'         => 'form-control',
            'style'         => 'overflow: hidden; word-wrap: break-word; height: 80px;',
        ]
    ])->textarea()?>
    

    <!-- //@TODO: implement pictures adding -->
    <div class="btn_add_picture">
        <img src="/img/icon_add_picture.png">
        <input type="file" name="avatar"><br>
    </div>

    <!-- //@TODO: show user's avatar here -->
    <div class="btn_send_comment"><img src="/img/icon_send_comment.png" style="cursor:pointer;"></div>

    
    <?=$form->field($model, 'parent_id', ['template' => '{input}'])->hiddenInput()?>
    <?=$form->field($model, 'original_parent_id', ['template' => '{input}'])->hiddenInput()?>

    <?php ActiveForm::end(); ?>

</div>

<?php 
    $this->registerJS(<<<JS
    $('#create-comment-form textarea').keydown(function (e) 
    {
      if (e.ctrlKey && e.keyCode == 13) 
        $('.btn_send_comment').click();
    });
JS
                      );
 ?>