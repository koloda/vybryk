<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "static_page".
 *
 * @property integer $id
 * @property string $title
 * @property string $content
 *
 * @property StaticPageMetaTags[] $staticPageMetaTags
 */
class StaticPage extends \yii\db\ActiveRecord
{
    /**
     * @var array
     */
    private $newMetaTags;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'static_page';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['content'], 'string'],
            [['title'], 'string', 'max' => 255],
            [
                ['title'],
                'match',
                'pattern' => '/^([а-яА-ЯЪъёЁїЇґҐ\'a-zA-Z0-9\.\,\; \’\-\/\\\(\)\*\+ *]+)$/u',
                'message' => 'Поле містить заборонені символи. Дозволені символи: а-я А-Я a-z A-Z \' . , / \ ( ) ; ’ - * +, а також символ пробілу.'
            ],
            [['title'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Назва',
            'content' => 'Контент',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStaticPageMetaTags()
    {
        return $this->hasMany(StaticPageMetaTag::className(), ['static_page_id' => 'id']);
    }
}
