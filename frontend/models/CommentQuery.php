<?php
namespace frontend\models;

use yii\db\ActiveQuery;

class CommentQuery extends ActiveQuery
{

    /**
     * @param $time
     * @return $this
     */
    public function timeRange($time)
    {
        $this->andWhere(['active' => $time]);

        return $this;
    }
}