<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "post".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $title
 * @property string $description
 * @property string $text
 * @property string $created
 * @property string $careated_date 
 * @property integer $comments_count
 * @property integer $pluses_count
 * @property integer $minuses_count
 *
 * @property PlusMinus[] $plusMinuses 
 * @property User $user
 * @property TagToPost[] $tagToPosts 
 * @property Comment[] $comments
 * @property Favorites[] $favorites
 */
class Post extends \yii\db\ActiveRecord
{
    /**
     * tags here
     */
    public $tags;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'post';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'title', 'text', 'careated_date', 'tags'], 'required'],
            [['user_id', 'comments_count', 'pluses_count', 'minuses_count'], 'integer'],
            [['text'], 'string'],
            [['created', 'careated_date'], 'safe'],
            [['title'], 'string', 'max' => 250],
            [['description'], 'string', 'max' => 400],
            [['title', 'text', 'description'], 'trim'],
            [['title', 'text', 'description'], 'filter', 'filter' => 'strip_tags'],
            [['tags'], 'checkTags']
        ];
    }

    /**
     * validate tags
     */
    public function checkTags()
    {
        if(is_array($this->tags))
        {
            if(count($this->tags) < 2)
                $this->addError('tags', Yii::t('app', 'You must add minimum 2 tags.'));
                
            if(count($this->tags) > 8)
                $this->addError('tags', Yii::t('app', 'You can add maximum 8 tags.'));
        }
        else
            $this->addError('tags', Yii::t('app', 'Some unknown error with tags'));
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'title' => 'Title',
            'description' => 'Description',
            'text' => 'Text',
            'created' => 'Created',
            'comments_count' => 'Comments Count',
            'pluses_count' => 'Pluses Count',
            'minuses_count' => 'Minuses Count',
        ];
    }

    /**
     * return list of avaliable scenarios
     */
    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios['create_text'] = ['title', 'description', 'text', 'tags'];
        return $scenarios;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

   /** 
    * @return \yii\db\ActiveQuery 
    */ 
   public function getTagToPosts() 
   { 
       return $this->hasMany(TagToPost::className(), ['post_id' => 'id']); 
   }

   /**
    * @return \yii\db\ActiveQuery
    */
   public function getPlusMinuses()
   {
       return $this->hasMany(PlusMinus::className(), ['post_id' => 'id']);
   }

    public function getPostTags()
    {
        return $this->hasMany(Tag::className(), ['id' => 'tag_id'])
            ->viaTable('tag_to_post', ['post_id' => 'id']);
    }

   public function getComments()
   {
       return $this->hasMany(Comment::className(), ['post_id' => 'id']);
   }

   public function getCommentsTree()
   {
       return $this->hasMany(Comment::className(), ['post_id' => 'id'])->where(['parent_id' => null]);
   }
}
