<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "plus_minus".
 *
 * @property integer $post_id
 * @property integer $user_id
 * @property string $sign
 *
 * @property User $user
 * @property Post $post
 */
class PlusMinus extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'plus_minus';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['post_id', 'user_id', 'sign'], 'required'],
            [['post_id', 'user_id'], 'integer'],
            [['sign'], 'string'],
            [['post_id', 'user_id'], 'unique', 'targetAttribute' => ['post_id', 'user_id'], 'message' => 'The combination of Post ID and User ID has already been taken.']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'post_id' => 'Post ID',
            'user_id' => 'User ID',
            'sign' => 'Sign',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPost()
    {
        return $this->hasOne(Post::className(), ['id' => 'post_id']);
    }
}
