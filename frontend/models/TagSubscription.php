<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "tag_subscription".
 *
 * @property integer $tag_id
 * @property integer $user_id
 *
 * @property User $user
 * @property Tag $tag
 */
class TagSubscription extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tag_subscription';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tag_id', 'user_id'], 'required'],
            [['tag_id', 'user_id'], 'integer'],
            [['tag_id', 'user_id'], 'unique', 'targetAttribute' => ['tag_id', 'user_id'], 'message' => 'The combination of Tag ID and User ID has already been taken.']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'tag_id' => 'Tag ID',
            'user_id' => 'User ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTag()
    {
        return $this->hasOne(Tag::className(), ['id' => 'tag_id']);
    }
}
