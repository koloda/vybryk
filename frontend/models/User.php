<?php
namespace frontend\models;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use yii\helpers\Url;

use dektrium\user\models\User as BaseUser;

/**
 * User model
 *
 * @property integer $id
 * @property string $username
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property string $auth_key
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $password write-only password
 *  
 * @property Post[] $favorites
 * @property Comment[] $favoriteComments
 * @property Post[] $posts
 * @property PlusMinus[] $plusMinuses
 * @property CommentPlusMinus[] $commentPlusMinuses
 * @property Profile $profile
 * @property SocialAccount[] $socialAccounts
 * @property Token[] $tokens
 * @property PlusMinus[] $pluses
 * @property PlusMinus[] $minuses
 * @property CommentPlusMinus[] $commentPluses
 * @property CommentPlusMinus[] $commentMinuses
 * @property Comment[] $comments
 */
class User extends BaseUser implements IdentityInterface
{
    public $postAddingError;

    private $_pluses;
    private $_minuses;
    private $_commentPluses;
    private $_commentMinuses;

    private $_favorites;
    private $_favoriteComments;

    private $_tag_subscriptions;
    private $_tag_ignores;

    private $_user_subscriptions;
    private $_user_ignores;

    public function canAddPost($type)
    {
        $maxPostsPerDay = 0;

        if($this->profile->rating >= 0)
        {
            $maxPostsPerDay = DEFAULT_POSTS_PER_DAY;

            if($this->profile->rating > THIRD_LEVEL_RATING)
                $maxPostsPerDay = THIRD_LEVEL_POSTS_PER_DAY;
        }
        else
        {
            $this->postAddingError = Yii::t('app', 'You can not add any posts (<0).');
            return false;
        }

        if($this->todayPostsCount() >= $maxPostsPerDay)
        {
            $this->postAddingError = Yii::t('app', 'Today posts limit is empty ({0, number})', $maxPostsPerDay);
            return false;
        }

        return true;
    }

    public function canAddComment()
    {
        if($this->profile->rating <= FIRST_SUBLEVEL_RATING)
            return false;

        return true;
    }

    public function canRate()
    {
        if($this->profile->rating <= SECOND_SUBLEVEL_RATING)
            return false;

        return true;
    }

    public function todayPostsCount()
    {
        return Post::find()
            ->where('careated_date = CURDATE()')
            ->andWhere(['user_id' => $this->id])
            ->count();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlusMinuses()
    {
        return $this->hasMany(PlusMinus::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCommentPlusMinuses()
    {
        return $this->hasMany(CommentPlusMinus::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPosts()
    {
        return $this->hasMany(Post::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFavorites()
    {
        return $this->hasMany(Post::className(), ['id' => 'post_id'])
            ->viaTable('favorites', ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFavoriteComments()
    {
        return $this->hasMany(Comment::className(), ['id' => 'comment_id'])
            ->viaTable('favorite_comments', ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getComments()
    {
       return $this->hasMany(Comment::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTagSubscriptions()
    {
        return $this->hasMany(Tag::className(), ['id' => 'tag_id'])
            ->viaTable('tag_subscription', ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTagIgnores()
    {
        return $this->hasMany(Tag::className(), ['id' => 'tag_id'])
            ->viaTable('tag_ignore', ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserSubscriptions()
    {
        return $this->hasMany(User::className(), ['id' => 'user_id'])
            ->viaTable('user_subscription', ['follower_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserIgnores()
    {
        return $this->hasMany(User::className(), ['id' => 'ignored_id'])
            ->viaTable('user_ignore', ['user_id' => 'id']);
    }

    public function getPluses()
    {
         if($this->_pluses == null)
             $this->processPlusesMinuses();
 
         return $this->_pluses;
    }

   public function getMinuses()
   {
        if($this->_minuses == null)
            $this->processPlusesMinuses();

        return $this->_minuses;
   }

   public function getCommentPluses()
    {
         if($this->_commentPluses == null)
             $this->processCommentPlusesMinuses();
 
         return $this->_commentPluses;
    }

   public function getCommentMinuses()
   {
        if($this->_commentMinuses == null)
            $this->processCommentPlusesMinuses();

        return $this->_commentMinuses;
   }

    private function processPlusesMinuses()
    {
         $this->_pluses = [];
         $this->_minuses = [];

         foreach ($this->plusMinuses as $pm) 
         {
             if($pm->sign == 'plus')
                 $this->_pluses[$pm->post_id] = $pm;
             elseif($pm->sign == 'minus')
                 $this->_minuses[$pm->post_id] = $pm;
         }
    }

    private function processCommentPlusesMinuses()
    {
         $this->_commentPluses = [];
         $this->_commentMinuses = [];

         foreach ($this->commentPlusMinuses as $pm) 
         {
             if($pm->sign == 'plus')
                 $this->_commentPluses[$pm->comment_id] = $pm;
             elseif($pm->sign == 'minus')
                 $this->_commentMinuses[$pm->comment_id] = $pm;
         }
    }

    public function getUserFavorites()
    {
         if($this->_favorites == null)
             $this->processFavorites();
 
         return $this->_favorites;
    }

    public function getUserTagSubscriptions()
    {
         if($this->_tag_subscriptions == null)
             $this->processTagSubscriptions();
 
         return $this->_tag_subscriptions;
    }

    public function getUserTagIgnores()
    {
         if($this->_tag_ignores == null)
             $this->processTagIgnores();
 
         return $this->_tag_ignores;
    }

    public function getUserUserSubscriptions()
    {
         if($this->_user_subscriptions == null)
             $this->processUserSubscriptions();
 
         return $this->_user_subscriptions;
    }

    public function getUserUserIgnores()
    {
         if($this->_user_ignores == null)
             $this->processUserIgnores();
 
         return $this->_user_ignores;
    }

    public function getUserFavoriteComments()
    {
         if($this->_favoriteComments == null)
             $this->processFavoriteComments();
 
         return $this->_favoriteComments;
    }

    public function getProfileUrl()
    {
        return Url::to(['/profile/user', 'id' => $this->id]);
    }

    public function getAvaUrl()
    {
        return '/img/img_3.png';
    }

    public function getRatedPosts()
    {
        $uid = $this->id;

        return Post::find()
            ->join('RIGHT JOIN', 'plus_minus', "post.id = plus_minus.post_id AND plus_minus.user_id = $uid");
    }

    private function processFavorites()
    {
        $this->_favorites = [];

        foreach ($this->favorites as $f)
            $this->_favorites[$f->id] = $f;
    }

    private function processFavoriteComments()
    {
        $this->_favoriteComments = [];

        foreach ($this->favoriteComments as $f)
            $this->_favoriteComments[$f->id] = $f;
    }

    private function processTagSubscriptions()
    {
        $this->_tag_subscriptions = [];

        foreach ($this->tagSubscriptions as $t)
            $this->_tag_subscriptions[$t->id] = $t;
    }

    private function processTagIgnores()
    {
        $this->_tag_ignores = [];

        foreach ($this->tagIgnores as $t)
            $this->_tag_ignores[$t->id] = $t;
    }

    private function processUserSubscriptions()
    {
        $this->_user_subscriptions = [];

        foreach ($this->userSubscriptions as $t)
            $this->_user_subscriptions[$t->user_id] = $t;
    }

    private function processUserIgnores()
    {
        $this->_user_ignores = [];

        foreach ($this->userIgnores as $t)
            $this->_user_ignores[$t->ignored_id] = $t;
    }


}