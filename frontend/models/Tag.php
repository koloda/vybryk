<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "tag".
 *
 * @property integer $id
 * @property string $text
 * @property integer $posts_count
 *
 * @property TagToPost[] $tagToPosts
 */
class Tag extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tag';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['text'], 'required'],
            [['posts_count'], 'integer'],
            [['text'], 'string', 'max' => 128, 'min' => 2],
            [['text'], 'filter', 'filter' => function ($v) {return trim(strip_tags($v));}]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'Tag ID'),
            'text' => Yii::t('app', 'Tag text'),
            'posts_count' => Yii::t('app', 'Tag posts count'),
        ];
    }

    /**
     * @inheritdoc
     * @return TagQuery
     */
    public static function find()
    {
        return new TagQuery(get_called_class());
    }

    public static function findRecentTop($days, $all = false)
    {
        return self::find()->recentTopQuery($days)->{$all ? 'all' : 'one'}();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTagToPosts()
    {
        return $this->hasMany(TagToPost::className(), ['tag_id' => 'id']);
    }

    public function getTagPosts()
    {
        return $this->hasMany(Post::className(), ['id' => 'post_id'])
            ->viaTable('tag_to_post', ['tag_id' => 'id']);
    }

}
