<?php
namespace frontend\models;

use yii\db\ActiveQuery;

class TagQuery extends ActiveQuery
{

    public function hoursRange($days)
    {
        $this->joinWith('tagToPosts', false, 'INNER JOIN')
            ->where('DATE(`attach_time`) between DATE_SUB(DATE(NOW()), INTERVAL :_days DAY) and DATE(NOW())', [':_days' => $days]);

        return $this;
    }

    public function orderByPopularity()
    {
        $this->orderBy('posts_count DESC');

        return $this;
    }

    public function recentTopQuery($days)
    {
        $this->hoursRange($days)->orderByPopularity();

        return $this;
    }
}