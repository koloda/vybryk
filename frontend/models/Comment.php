<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "comment".
 *
 * @property integer $id
 * @property integer $post_id
 * @property integer $user_id
 * @property integer $parent_id
 * @property string $text
 * @property string $created
 * @property integer $pluses_count
 * @property integer $minuses_count
 *
 * @property User $user
 * @property Post $post
 */
class Comment extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'comment';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['post_id', 'user_id', 'text'], 'required'],
            [['post_id', 'user_id', 'parent_id', 'original_parent_id', 'pluses_count', 'minuses_count'], 'integer'],
            [['text'], 'string'],
            [['text'], 'trim'],
            [['text'], 'filter', 'filter' => 'strip_tags'],
            [['created'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'post_id' => Yii::t('app', 'Post ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'parent_id' => Yii::t('app', 'Parent ID'),
            'text' => Yii::t('app', 'Text'),
            'created' => Yii::t('app', 'Created'),
            'pluses_count' => Yii::t('app', 'Pluses Count'),
            'minuses_count' => Yii::t('app', 'Minuses Count'),
        ];
    }

    /**
     * @inheritdoc
     * @return CommentQuery
     */
    public static function find()
    {
        return new CommentQuery(get_called_class());
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPost()
    {
        return $this->hasOne(Post::className(), ['id' => 'post_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getChilds()
    {
       return $this->hasMany(Comment::className(), ['parent_id' => 'id']);
    }


    /**
     * ADDITIONAL FUNCTIONS HERE
     */

    /**
     * Return username of comment author
     */
    public function getAuthorName()
    {
        return $this->user->username;
    }

/*    public static function findTopComment()
    {
        return self::find()->where(['pluses_count' => 'MAX(pluses_count)'])->one();
    }*/
}
