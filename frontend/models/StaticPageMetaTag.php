<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "static_page_meta_tags".
 *
 * @property integer $id
 * @property string $name
 * @property string $meta
 * @property integer $static_page_id
 *
 * @property StaticPage $staticPage
 */
class StaticPageMetaTag extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'static_page_meta_tags';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'meta', 'static_page_id'], 'required'],
            [['static_page_id'], 'integer'],
            [['name', 'meta'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'meta' => 'Meta'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStaticPage()
    {
        return $this->hasOne(StaticPage::className(), ['id' => 'static_page_id']);
    }
}
