<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "user_subscription".
 *
 * @property integer $user_id
 * @property integer $follower_id
 *
 * @property User $user
 * @property User $follower
 */
class UserSubscription extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_subscription';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'follower_id'], 'required'],
            [['user_id', 'follower_id'], 'integer'],
            [['follower_id', 'user_id'], 'unique', 'targetAttribute' => ['follower_id', 'user_id'], 'message' => 'The combination of User ID and Follower ID has already been taken.']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'user_id' => 'User ID',
            'follower_id' => 'Follower ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFollower()
    {
        return $this->hasOne(User::className(), ['id' => 'follower_id']);
    }
}
