<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "user_ignore".
 *
 * @property integer $ignored_id
 * @property integer $user_id
 *
 * @property User $user
 * @property User $ignored
 */
class UserIgnore extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_ignore';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ignored_id', 'user_id'], 'required'],
            [['ignored_id', 'user_id'], 'integer'],
            [['ignored_id', 'user_id'], 'unique', 'targetAttribute' => ['ignored_id', 'user_id'], 'message' => 'The combination of Ignored ID and User ID has already been taken.']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ignored_id' => 'Ignored ID',
            'user_id' => 'User ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIgnored()
    {
        return $this->hasOne(User::className(), ['id' => 'ignored_id']);
    }
}
