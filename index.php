<?php
defined('YII_DEBUG') or define('YII_DEBUG', true);
defined('YII_ENV') or define('YII_ENV', 'dev');

/**
 * APPLICATION CONSTANTS HERE
 */

define('DEFAULT_POSTS_PER_DAY', 1);
define('FIRST_SUBLEVEL_RATING', -25);
define('SECOND_SUBLEVEL_RATING', -75);
define('THIRD_SUBLEVEL_RATING', -100);
define('FIRST_LEVEL_RATING', 100);
define('SECOND_LEVEL_RATING', 150);
define('THIRD_LEVEL_RATING', 1000);
define('FOURTH_LEVEL_RATING', 10000);
define('THIRD_LEVEL_POSTS_PER_DAY', 5);

define('POSTS_PER_PAGE', 10);

/**
 * END OF PREDEFINED APP CONSTANTS
 */

require(__DIR__ . '/../../vendor/autoload.php');
require(__DIR__ . '/../../vendor/yiisoft/yii2/Yii.php');
require(__DIR__ . '/../../common/config/bootstrap.php');
require(__DIR__ . '/../config/bootstrap.php');

$config = yii\helpers\ArrayHelper::merge(
    require(__DIR__ . '/../../common/config/main.php'),
    require(__DIR__ . '/../../common/config/main-local.php'),
    require(__DIR__ . '/../config/main.php'),
    require(__DIR__ . '/../config/main-local.php')
);

$application = new yii\web\Application($config);
$application->run();
